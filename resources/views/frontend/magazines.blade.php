@extends('layouts.template.frontend')


<style>
    #ju-container .ju-page-title {
        margin-top: 179px;
    }

</style>
@section('content')
    <div id="ju-container">
        <div id="ju-content" class="container">
            <div class="ju-page-title">
                <h1 class="entry-title text-gotham text-center">Brand JSM</h1>
                <div class="text-center text-gotham">Test</div>
            </div>


            <ul class="nav nav-tabs">
                <li class="active"><a href="/brand" class="letters">Concept</a></li>
                <li><a href="/brand/magazines" class="letters">Magazines</a></li>
            </ul>



            <div id="bbsData">
                <div class="page-body">
                    <div class="bbs-table-list tabs-wrap">
                        <table summary="제목, 작성일, 조회수, 동영상">
                            <caption class="displaynone">Test</caption>
                            <colgroup>
                                <col width="*" />
                                <col width="110" />
                                <col width="60" />
                            </colgroup>
                            <tbody>
                            </tbody>
                        </table>
                        <div class="video-list">
                            <table summary="제목, 작성일, 조회수, 동영상">
                                <colgroup>
                                    <col width="285px" />
                                    <col width="285px" />
                                    <col width="285px" />
                                    <col width="285px" />
                                </colgroup>
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="box">
                                            <a href="/board/board.html?code=jsmbeauty_image2&page=1&type=v&board_cate=3&num1=999593&num2=00000&number=195&lock=N" class="video_link">
                                                <div class="video_txt">
                                                    <h5 class="letters">[Magazines] 얼루어 2019년 08월호</h5>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024" src="https://board.makeshop.co.kr/board/premium236/jsmbeauty_image2/20190809094216.jpg" class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="/board/board.html?code=jsmbeauty_image2&page=1&type=v&board_cate=3&num1=999594&num2=00000&number=194&lock=N" class="video_link">
                                                <div class="video_txt">
                                                    <h5 class="letters">[Magazines] 앳스타일 2019년 08월호</h5>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024" src="https://board.makeshop.co.kr/board/premium236/jsmbeauty_image2/20190809094139.jpg" class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="/board/board.html?code=jsmbeauty_image2&page=1&type=v&board_cate=3&num1=999595&num2=00000&number=193&lock=N" class="video_link">
                                                <div class="video_txt">
                                                    <h5 class="letters">[Magazines] 뷰티쁠 2019년 08월호</h5>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024" src="https://board.makeshop.co.kr/board/premium236/jsmbeauty_image2/20190809094102.jpg" class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="/board/board.html?code=jsmbeauty_image2&page=1&type=v&board_cate=3&num1=999596&num2=00000&number=192&lock=N" class="video_link">
                                                <div class="video_txt">
                                                    <h5 class="letters">[Magazines] 그라피 2019년 08월호</h5>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024" src="https://board.makeshop.co.kr/board/premium236/jsmbeauty_image2/20190809094040.jpg" class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                </tr><tr>
                                    <td>
                                        <div class="box">
                                            <a href="/board/board.html?code=jsmbeauty_image2&page=1&type=v&board_cate=3&num1=999604&num2=00000&number=191&lock=N" class="video_link">
                                                <div class="video_txt">
                                                    <h5 class="letters">[Magazines] 뷰티쁠 2019년 07월호</h5>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024" src="https://board.makeshop.co.kr/board/premium236/jsmbeauty_image2/20190702114159.jpg" class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="/board/board.html?code=jsmbeauty_image2&page=1&type=v&board_cate=3&num1=999605&num2=00000&number=190&lock=N" class="video_link">
                                                <div class="video_txt">
                                                    <h5 class="letters">[Magazines] 싱글즈 2019년 07월호</h5>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024" src="https://board.makeshop.co.kr/board/premium236/jsmbeauty_image2/20190702114127.jpg" class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="/board/board.html?code=jsmbeauty_image2&page=1&type=v&board_cate=3&num1=999606&num2=00000&number=189&lock=N" class="video_link">
                                                <div class="video_txt">
                                                    <h5 class="letters">[Magazines] 퍼스트룩 2019년 07월호</h5>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024" src="https://board.makeshop.co.kr/board/premium236/jsmbeauty_image2/20190626145827.jpg" class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="/board/board.html?code=jsmbeauty_image2&page=1&type=v&board_cate=3&num1=999607&num2=00000&number=188&lock=N" class="video_link">
                                                <div class="video_txt">
                                                    <h5 class="letters">[Magazines] 코스모폴리탄 2019년 07월호</h5>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024" src="https://board.makeshop.co.kr/board/premium236/jsmbeauty_image2/20190624171155.jpg" class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                </tr><tr>
                                    <td>
                                        <div class="box">
                                            <a href="/board/board.html?code=jsmbeauty_image2&page=1&type=v&board_cate=3&num1=999608&num2=00000&number=187&lock=N" class="video_link">
                                                <div class="video_txt">
                                                    <h5 class="letters">[Magazines] 보그 2019년 07월호</h5>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024" src="https://board.makeshop.co.kr/board/premium236/jsmbeauty_image2/20190624171125.jpg" class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="/board/board.html?code=jsmbeauty_image2&page=1&type=v&board_cate=3&num1=999609&num2=00000&number=186&lock=N" class="video_link">
                                                <div class="video_txt">
                                                    <h5 class="letters">[Magazines] 바자 2019년 07월호</h5>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024" src="https://board.makeshop.co.kr/board/premium236/jsmbeauty_image2/20190624171051.jpg" class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="/board/board.html?code=jsmbeauty_image2&page=1&type=v&board_cate=3&num1=999610&num2=00000&number=185&lock=N" class="video_link">
                                                <div class="video_txt">
                                                    <h5 class="letters">[Magazines] 얼루어 2019년 07월호</h5>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024" src="https://board.makeshop.co.kr/board/premium236/jsmbeauty_image2/20190624171008.jpg" class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="/board/board.html?code=jsmbeauty_image2&page=1&type=v&board_cate=3&num1=999611&num2=00000&number=184&lock=N" class="video_link">
                                                <div class="video_txt">
                                                    <h5 class="letters">[Magazines] 투브라이드 2019년 06월호</h5>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024" src="https://board.makeshop.co.kr/board/premium236/jsmbeauty_image2/20190610141432.jpg" class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                </tr><tr>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div><!--.bbs-table-list-->
                    <div class="bbs-sch displaynone">
                        <form action="board.html" name="form1">
                            <input type=hidden name=s_id value="">
                            <input type=hidden name=code value="jsmbeauty_image2">
                            <input type=hidden name=page value=1>
                            <input type=hidden name=type value=s>
                            <input type=hidden name=board_cate value="3">
                            <input type=hidden name="review_type" value="" />                    <fieldset>
                                <legend class="displaynone">게시판 검색 폼</legend>
                                <label>
                                    <select name="search_type" class="brd-st">                <option value="">선택</option>                <option value="hname">이름</option>
                                        <option value="subject">제목</option>
                                        <option value="content">내용</option>

                                    </select>                        </label>
                                <span class="key-wrap">
                            <input type='text' name='stext' value=''  class="MS_input_txt" />                            <a href="javascript:document.form1.submit();" class="btn btn-gray"><i class="fa fa-search"></i> 검색</a>
                        </span>
                            </fieldset>
                        </form>                </div><!-- .bbs-sch -->
                    <dl class="bbs-link bbs-link-btm displaynone">
                        <dd>
                            <a class="write" href="/board/board.html?code=jsmbeauty_image2&page=1&board_cate=3&type=i">글쓰기</a>
                        </dd>
                    </dl>
                    <nav class="text-center">
                        <ul class="pagination">
                            <li><span class="page-numbers current">1</span></li>
                            <li><a class="page-numbers">2</a></li>
                            <li><a class="page-numbers">3</a></li>
                            <li><a class="page-numbers">4</a></li>
                            <li><a class="page-numbers">5</a></li>
                            <li><a class="page-numbers">6</a></li>
                            <li><a class="page-numbers">7</a></li>
                            <li><a class="page-numbers">8</a></li>
                            <li><a class="page-numbers">11</a></li>
                            <li><a class="page-numbers">12</a></li>
                        </ul>
                    </nav>
                </div>
            </div><!-- .page-body -->

        </div><!--#ju-content-->
    </div><!--#ju-container-->
    <!-- //content -->


@endsection


