@extends('layouts.template.frontend')

<style type="text/css">
#ju-container .ju-page-title {
    margin-top: 179px;
}
</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM PRODUCT </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>

@section('content')
<div id="ju-container">
    <div id="ju-content" class="container">
        <div class="ju-page-title" id="demo">
            <h1 class="entry-title text-gotham text-center">- ALL PRODUCTS -</h1>
        </div>
        <div class="woocommerce_categoy_result clearfix">
            <p class="form-control-static pull-left">
                TOTAL <strong class="text-danger" id="total">16</strong>PRODUCTS
            </p>

            <div class="pull-right">
                <form class="woocommerce-ordering" method="get">
                    <label class="">
                        <select id="selArray" name="selArray" class="orderby form-control">
                            <option value="">Lowest Price</option>
                            <option value="">Highest Price</option>
                            <option value="">Product Name</option>
                            <option value="" selected="selected">New Item</option>
                        </select>
                    </label>
                </form>
            </div>
        </div>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#product" onclick="products(event, 'all')">PRODUCTS</a></li>
            <li><a data-toggle="tab" href="#best" onclick="products(event, 'best')">BEST SELLER</a></li>
            <li><a data-toggle="tab" href="#base" onclick="products(event, 'base')">BASE</a></li>
            <li><a data-toggle="tab" href="#lip" onclick="products(event, 'lip')">LIP</a></li>
            <li><a data-toggle="tab" href="#eye" onclick="products(event, 'eye')">EYE</a></li>
            <li><a data-toggle="tab" href="#skincare" onclick="products(event, 'skincare')">SKINCARE</a></li>
            <li><a data-toggle="tab" href="#tool" onclick="products(event, 'tool')">TOOL</a></li>
        </ul>

        <div class="tab-content">
            <div id="product" class="tab-pane fade in active">
                <div id="all" class="product">
                    <div class="row">
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/1">
                                <img src="{!!asset('jsmbeauty/src/product/product_1.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Pre-tect Sun Waterfull
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/2">
                                <img src="{!!asset('jsmbeauty/src/product/product_2.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Artist Contour Palette
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/3">
                                <img src="{!!asset('jsmbeauty/src/product/product_3.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL High Tinted Lip Lacquer( Rose Cosset)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/4">
                                <img src="{!!asset('jsmbeauty/src/product/product_4.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Lip-pression #JUST RED
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/5">
                                <img src="{!!asset('jsmbeauty/src/product/product_5.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Setting Glowing Base
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/6">
                                <img src="{!!asset('jsmbeauty/src/product/product_6.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Setting Smoothing Base
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/7">
                                <img src="{!!asset('jsmbeauty/src/product/product_7.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Essential Star-cealer Foundation (Fair Light)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/8">
                                <img src="{!!asset('jsmbeauty/src/product/product_8.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Essential Skin Nuder Cushion (Fair Light)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/9">
                                <img src="{!!asset('jsmbeauty/src/product/product_9.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Skin Setting Tone Manner Base SPF 30/ PA+++
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/10">
                                <img src="{!!asset('jsmbeauty/src/product/product_10.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        Essential Skin Nuder Longwear Cushion (Fair Light)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/11">
                                <img src="{!!asset('jsmbeauty/src/product/product_11.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Cushion-cealer (Fair Light)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/12">
                                <img src="{!!asset('jsmbeauty/src/product/product_12.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Refining Color-bony Brow (Light-bony)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/13">
                                <img src="{!!asset('jsmbeauty/src/product/product_13.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Artist Make-up Blending Tool
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/14">
                                <img src="{!!asset('jsmbeauty/src/product/product_14.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Artist Brush Foundation
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/15">
                                <img src="{!!asset('jsmbeauty/src/product/product_15.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        Artist Brush Contour
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/16">
                                <img src="{!!asset('jsmbeauty/src/product/product_16.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        Artist Brush Hair Line
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                    </div>
                </div>
            </div>
            <div id="best" class="tab-pane fade">
                <div id="all" class="product">
                    <div class="row">
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/1">
                                <img src="{!!asset('jsmbeauty/src/product/product_1.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Pre-tect Sun Waterfull
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/2">
                                <img src="{!!asset('jsmbeauty/src/product/product_2.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Artist Contour Palette
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/3">
                                <img src="{!!asset('jsmbeauty/src/product/product_3.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL High Tinted Lip Lacquer( Rose Cosset)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/4">
                                <img src="{!!asset('jsmbeauty/src/product/product_4.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Lip-pression #JUST RED
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/5">
                                <img src="{!!asset('jsmbeauty/src/product/product_5.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Setting Glowing Base
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/6">
                                <img src="{!!asset('jsmbeauty/src/product/product_6.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Setting Smoothing Base
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/7">
                                <img src="{!!asset('jsmbeauty/src/product/product_7.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Essential Star-cealer Foundation (Fair Light)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/8">
                                <img src="{!!asset('jsmbeauty/src/product/product_8.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Essential Skin Nuder Cushion (Fair Light)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/9">
                                <img src="{!!asset('jsmbeauty/src/product/product_9.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Skin Setting Tone Manner Base SPF 30/ PA+++
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/10">
                                <img src="{!!asset('jsmbeauty/src/product/product_10.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        Essential Skin Nuder Longwear Cushion (Fair Light)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/11">
                                <img src="{!!asset('jsmbeauty/src/product/product_11.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Cushion-cealer (Fair Light)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/12">
                                <img src="{!!asset('jsmbeauty/src/product/product_12.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Refining Color-bony Brow (Light-bony)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/13">
                                <img src="{!!asset('jsmbeauty/src/product/product_13.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Artist Make-up Blending Tool
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/14">
                                <img src="{!!asset('jsmbeauty/src/product/product_14.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Artist Brush Foundation
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/15">
                                <img src="{!!asset('jsmbeauty/src/product/product_15.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        Artist Brush Contour
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <a class="product_link_wrap" href="/product/detail/view/16">
                                <img src="{!!asset('jsmbeauty/src/product/product_16.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        Artist Brush Hair Line
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                    </div>
                </div>
            </div>

            <div id="base" class="tab-pane fade">
                <div id="all" class="product">
                    <div class="row">
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/2">
                                <img src="{!!asset('jsmbeauty/src/product/product_2.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Artist Contour Palette
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/5">
                                <img src="{!!asset('jsmbeauty/src/product/product_5.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Setting Glowing Base
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/6">
                                <img src="{!!asset('jsmbeauty/src/product/product_6.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Setting Smoothing Base
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/7">
                                <img src="{!!asset('jsmbeauty/src/product/product_7.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Essential Star-cealer Foundation (Fair Light)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/8">
                                <img src="{!!asset('jsmbeauty/src/product/product_8.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Essential Skin Nuder Cushion (Fair Light)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/9">
                                <img src="{!!asset('jsmbeauty/src/product/product_9.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Skin Setting Tone Manner Base SPF 30/ PA+++
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/10">
                                <img src="{!!asset('jsmbeauty/src/product/product_10.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Essential Skin Nuder Longwear Cushion (Fair Light)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/11">
                                <img src="{!!asset('jsmbeauty/src/product/product_11.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Cushion-cealer (Fair Light)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                    </div>
                </div>
            </div>
            <div id="lip" class="tab-pane fade">
                <div id="all" class="product">
                    <div class="row">
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/3">
                                <img src="{!!asset('jsmbeauty/src/product/product_3.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL High Tinted Lip Lacquer( Rose Cosset)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/4">
                                <img src="{!!asset('jsmbeauty/src/product/product_4.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Lip-pression #JUST RED
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                    </div>
                </div>
            </div>
            <div id="eye" class="tab-pane fade">
                <div id="all" class="product">
                    <div class="row">
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/12">
                                <img src="{!!asset('jsmbeauty/src/product/product_12.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Refining Color-bony Brow (Light-bony)
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                    </div>
                </div>
            </div>
            <div id="skincare" class="tab-pane fade">
                <div id="all" class="product">
                    <div class="row">
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/1">
                                <img src="{!!asset('jsmbeauty/src/product/product_1.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Pre-tect Sun Waterfull
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                    </div>
                </div>
            </div>
            <div id="tool" class="tab-pane fade">
                <div id="all" class="product">
                    <div class="row">
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/13">
                                <img src="{!!asset('jsmbeauty/src/product/product_13.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Artist Make-up Blending Tool
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/14">
                                <img src="{!!asset('jsmbeauty/src/product/product_14.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        JUNGSAEMMOOL Artist Brush Foundation
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/15">
                                <img src="{!!asset('jsmbeauty/src/product/product_15.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        Artist Brush Contour
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                        <div class="col-md-3"><a class="product_link_wrap" href="/product/detail/view/16">
                                <img src="{!!asset('jsmbeauty/src/product/product_16.jpg')!!}" alt="product"
                                    style="width:100%;">
                            </a>
                            <div class="text-center prd-info">

                                <div class="preview-box clear bold">
                                    <span class="right"><span class="quick_basket" rel="1182" style="cursor:pointer"><i
                                                class="fa fa-shopping-cart"></i></span></span>
                                </div>

                                <h5><a href="">
                                        Artist Brush Hair Line
                                    </a></h5>
                                <p class="subname"></p>
                                <strong class="price">
                                    <span class="amount">
                                        <span>
                                        </span></span>
                                </strong>
                            </div>
                            <hr class="clear">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--#ju-content-->
</div>
<!--#ju-container-->
<!-- //content -->


@endsection

<script>
function products(evt, obj) {
    if (obj == 'all') {
        document.getElementById("demo").innerHTML = '  <div class="ju-page-title" id="t">\n' +
            '                <h1 class="entry-title text-gotham text-center">- ALL PRODUCTS -</h1>\n' +
            '            </div>';
        document.getElementById("total").innerHTML = '16';
    }
    if (obj == 'best') {
        document.getElementById("demo").innerHTML = '  <div class="ju-page-title" id="t">\n' +
            '                <h1 class="entry-title text-gotham text-center">- BEST SELLERS -</h1>\n' +
            '            </div>';
        document.getElementById("total").innerHTML = '16';
    }
    if (obj == 'base') {
        document.getElementById("demo").innerHTML = '  <div class="ju-page-title" id="t">\n' +
            '                <h1 class="entry-title text-gotham text-center">- BASE -</h1>\n' +

            '            </div>';
        document.getElementById("total").innerHTML = '8';
    }
    if (obj == 'lip') {
        document.getElementById("demo").innerHTML = '  <div class="ju-page-title" id="t">\n' +
            '                <h1 class="entry-title text-gotham text-center">- LIP -</h1>\n' +

            '            </div>';
        document.getElementById("total").innerHTML = '2';
    }
    if (obj == 'eye') {
        document.getElementById("demo").innerHTML = '  <div class="ju-page-title" id="t">\n' +
            '                <h1 class="entry-title text-gotham text-center">- EYE -</h1>\n' +

            '            </div>';
        document.getElementById("total").innerHTML = '1';
    }
    if (obj == 'skincare') {
        document.getElementById("demo").innerHTML = '  <div class="ju-page-title" id="t">\n' +
            '                <h1 class="entry-title text-gotham text-center">- SKINCARE -</h1>' +

            '            </div>';
        document.getElementById("total").innerHTML = '1';
    }
    if (obj == 'tool') {
        document.getElementById("demo").innerHTML = '  <div class="ju-page-title" id="t">\n' +
            '                <h1 class="entry-title text-gotham text-center">- TOOL -</h1>' +

            '            </div>';
        document.getElementById("total").innerHTML = '4';
    }

}
</script>