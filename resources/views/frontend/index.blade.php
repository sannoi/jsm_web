@extends('layouts.template.frontend')


<style type="text/css">
.carousel-indicators li {
    display: inline-block;
    box-sizing: content-box;
    width: 8px;
    height: 8px;
    background: black;
    border: solid 3px white;
    border-color: rgba(255, 255, 255, 0.44);
    margin: 4px;
    border-radius: 15px;
}
li.active {
    color: red;
}
</style>

@section('content')

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>

<div id="mobile-video" class="visible-xs-block"
    style="background-size: cover; background-position: center center; background-image: url(http://jsmbeauty.img18.kr/mobile/main/mobile_main_722%2D1080.jpg);">
    <div class="item-content">
        <div class="item-cell">
            <h1>Beauty Inspiration</h1>
        </div>
    </div>
</div>


<div id="video" class="hidden-xs" style="background-color: black">
    <div class="item-content">
        <div class="item-cell">
        </div>
    </div>

    <div id="video_bg" style="background-color: black;">
        <!-- <img src="/jsmbeauty/src/190807_jsm_fw_making.gif" > -->
        <iframe
            src="https://player.vimeo.com/video/333657595?title=0&byline=0&background=1&portrait=0&autoplay=1&loop=1"
            frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

    </div>
</div>

<div class="base-height"></div>
<!-- content -->
<div id="ju-container">
    <div id="ju-content">
        <div
            class="vc_row wpb_row vc_row-fluid vc_custom_1464829003762 vc_row-has-fill vc_row-o-full-height vc_row-o-columns-bottom vc_row-flex">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner vc_custom_1446006821477">
                    <div class="wpb_wrapper">
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill">
                                <div id="f2s-rolling-container1" class="f2s-swiper">

                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0"
                                                class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                                        </ol>


                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active" style="width: 100%;">
                                                <img src="jsmbeauty/src/Banner/banner-01.jpg">
                                            </div>
                                            <div class="item" style="width: 100%;">
                                                <img src="jsmbeauty/src/Banner/banner-02.jpg">
                                            </div>
                                            <div class="item" style="width: 100%;">
                                                <img src="jsmbeauty/src/Banner/banner-03.jpg">
                                            </div>
                                            <div class="item" style="width: 100%;">
                                                <img src="jsmbeauty/src/main_rolling1_02.jpg">
                                            </div>
                                            <div class="item" style="width: 100%;">
                                                <img src="jsmbeauty/src/main_rolling1_09.jpg">
                                            </div>
                                        </div>

                                        <!-- <a class="left carousel-control" href="#carousel-example-generic" role="button"
                                            data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button"
                                            data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a> -->
                                    </div>

                                </div>
                            </div>
                        </div>

                        <h1 style="font-size: 50px;color: #ffffff;line-height: 60px;text-align: center; padding-top: 40px !important;"
                            class="vc_custom_heading vc_custom_1467341574385">
                            <a href="">EVENT</a>
                        </h1>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1467342034471 vc_row-has-fill">
                            <div class="wpb_column vc_column_container vc_col-sm-3">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_single_image wpb_content_element vc_align_center">
                                            <figure class="wpb_wrapper vc_figure">
                                                <a href="" target="_self"
                                                    class="vc_single_image-wrapper vc_box_border_grey">
                                                    <img width="385" height="452"
                                                        src="{!!asset('jsmbeauty/src/event/main_01.jpg')!!}"
                                                        class="vc_single_image-img attachment-full" alt="event">

                                                </a>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-3">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_single_image wpb_content_element vc_align_center">
                                            <figure class="wpb_wrapper vc_figure">
                                                <a href="http://www.jsmbeauty.com/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=10&num1=999776&num2=00000&number=5&lock=N"
                                                    target="_self" class="vc_single_image-wrapper vc_box_border_grey">
                                                    <img width="385" height="452"
                                                        src="{!!asset('jsmbeauty/src/event/main_02.jpg')!!}"
                                                        class="vc_single_image-img attachment-full" alt="event">

                                                </a>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-3">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_single_image wpb_content_element vc_align_center">
                                            <figure class="wpb_wrapper vc_figure">
                                                <a href="" target="_self"
                                                    class="vc_single_image-wrapper vc_box_border_grey">
                                                    <img width="384" height="452"
                                                        src="{!!asset('jsmbeauty/src/event/main_03.jpg')!!}"
                                                        class="vc_single_image-img attachment-full" alt="event_03">
                                                </a>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-3">
                                <div class="vc_column-inner ">
                                    <div class="wpb_single_image wpb_content_element vc_align_center">
                                        <figure class="wpb_wrapper vc_figure">
                                            <a href="" target="_self"
                                                class="vc_single_image-wrapper vc_box_border_grey">
                                                <img width="385" height="452"
                                                    src="{!!asset('jsmbeauty/src/event/main_04.jpg')!!}"
                                                    class="vc_single_image-img attachment-full" alt="event">
                                            </a>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- .vc_row wpb_row vc_inner vc_row-fluid vc_custom_1467342034471 vc_row-has-fill-->

                </div>
            </div>
            <!--.vc_column-inner vc_custom_1446006821477-->
        </div>
        <!--.wpb_column vc_column_container vc_col-sm-12-->
    </div>
    <!--.vc_row wpb_row vc_row-fluid vc_custom_1464829003762 vc_row-has-fill vc_row-o-full-height vc_row-o-columns-bottom vc_row-flex-->


 

    <div class="vc_row wpb_row vc_row-fluid home-product-tab vc_custom_1467341696348 vc_row-has-fill">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <h1 style="font-size: 50px;color: #ffffff;text-align: center"
                        class="vc_custom_heading vc_custom_1467342620922"><a
                            href="/shop/shopbrand.html?xcode=008&amp;type=Y">PRODUCT</a></h1>
                    <div class="wpb_tabs wpb_content_element" data-interval="0">
                        <div
                            class="wpb_wrapper wpb_tour_tabs_wrapper ui-tabs vc_clearfix ui-widget ui-widget-content ui-corner-all">
                            <ul class="wpb_tabs_nav ui-tabs-nav vc_clearfix ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all"
                                role="tablist">
                                <li class="ui-state-default ui-corner-top ui-state-active navMenus" role="tab"
                                    tabindex="0" aria-controls="tab-1b1d9d2f-6ba7-0" aria-labelledby="ui-id-1"
                                    aria-selected="true" aria-expanded="true"><a  data-toggle="tab" href="#best"
                                        class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1" onclick="products(event, 'best')"> BEST
                                        PRODUCTS </a></li>
                                     

                                <li class="ui-state-default ui-corner-top navMenus" role="tab" tabindex="-1"
                                    aria-controls="tab-a2934e91-9508-3" aria-labelledby="ui-id-2" aria-selected="false"
                                    aria-expanded="false"><a data-toggle="tab" href="#base" onclick="products(event, 'base')">BASE</a></li>
                                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1"
                                    aria-controls="tab-1445999610524-2-2" aria-labelledby="ui-id-3"
                                    aria-selected="false" aria-expanded="false"><a data-toggle="tab" href="#lip">LIP</a>
                                </li>
                                <li class="ui-state-default ui-corner-top navMenus" role="tab" tabindex="-1"
                                    aria-controls="tab-1446117257244-4-6" aria-labelledby="ui-id-4"
                                    aria-selected="false" aria-expanded="false"><a data-toggle="tab" href="#eye">EYE</a>
                                </li>
                                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1"
                                    aria-controls="tab-1445999639965-3-2" aria-labelledby="ui-id-5"
                                    aria-selected="false" aria-expanded="false"><a data-toggle="tab" href="#skincare">SKINCARE</a></li>
                                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1"
                                    aria-controls="tab-1459485847092-5-5" aria-labelledby="ui-id-6"
                                    aria-selected="false" aria-expanded="false"><a data-toggle="tab" href="#tools">Tools</a>
                                </li>
                            </ul>
                             <div class="tab-content">
    <div id="best" class="tab-pane fade in active">
                                <div class="woocommerce columns-4">
                                    <div class="row row-products">
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/1">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_1.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="JUNGSAEMMOOL Pre-tect Sun Waterfull">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Pre-tect Sun Waterfull</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">SPF50+/ PA++++<br>
                                                        เนื้อบางเบา</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/2">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_2.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="JUNGSAEMMOOL Artist Contour Palette">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Artist Contour Palette</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">เนื้อสัมผัสที่นุ่มละมุน<br>
                                                        ด้วยส่วนผสมจากแร่ธาตุช่วยให้แต่งแต้มผิวได้อย่างเรียบรื่น
                                                        และผิวไม่มีจุดด่างพร้อย</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/3">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_3.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="JUNGSAEMMOOL High Tinted Lip Lacquer( Rose Cosset)">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL High Tinted Lip Lacquer( Rose Cosset)</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">ลิปแลคเกอร์ที่ให้เม็ดสีสดชัด<br>
                                                        เพิ่มความชุ่มชื้นให้ริมฝีปาก</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/4">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_4.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="Brush-Pouch-Set">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Lip-pression #JUST RED</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">สีชวนหลงใหล<br>
                                                        น่าดึงดูด</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/5">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_5.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="Brush-Pouch-Set">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Setting Glowing Base</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">
                                                        เบสที่ช่วยให้ผิวกระจ่างใสสุขภาพดี<br>
                                                        เหมาะสำหรับผิวแพ้ง่าย
                                                    </div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/6">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_6.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="Brush-Pouch-Set">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Setting Smoothing Base</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">
                                                        เมคอัพเบสที่ช่วยให้เครื่องสำอางติดทนนาน<br>
                                                        พร้อมควบคุมความมัน</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/7">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_7.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="Brush-Pouch-Set">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Essential Star-cealer Foundation (Fair Light)</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">
                                                        ประสิทธิภาพที่ประสานกันระหว่างรองพื้น <br>
                                                        และคอนซีลเลอร์!</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/8">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_8.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="Brush-Pouch-Set">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Essential Skin Nuder Cushion (Fair Light)</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">การปกปิดอย่างสมบูรณ์แบบ <br>
                                                        ผิวโกลว์ ฉ่ำวาว ยาวนาน </div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/9">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_9.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="JUNGSAEMMOOL Pre-tect Sun Waterfull">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Skin Setting Tone Manner Base SPF 30/ PA+++</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">เบสสำหรับผู้ชายออล อิน วัน<br>
                                                        ปกป้องแสงแดด</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/10">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_10.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="JUNGSAEMMOOL Pre-tect Sun Waterfull">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Essential Skin Nuder Longwear Cushion (Fair Light)
                                                    </h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">
                                                        รองพื้นคูชชั่นที่ให้เนื้อสัมผัสนุ่มละมุน<br>
                                                        ติดทนนาน </div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/11">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_11.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="JUNGSAEMMOOL Pre-tect Sun Waterfull">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Cushion-cealer (Fair Light)</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">คูชชั่นที่เน้นการปกปิด <br>
                                                        พร้อมด้วย SPF 50+ / PA+++</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/12">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_12.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="JUNGSAEMMOOL Pre-tect Sun Waterfull">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Refining Color-bony Brow (Light-bony)</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">ดุจดั่งปลายดินสอของศิลปิน Refining
                                                        Color Bony Brow<br>
                                                        ช่วยให้คุณเขียนคิ้วได้สวยเฉียบดั่งใจ</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/13">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_13.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="JUNGSAEMMOOL Pre-tect Sun Waterfull">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Artist Make-up Blending Tool</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">สะดวกสบาย<br>
                                                        และหลากหลายประโยชน์ </div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/14">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_14.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="JUNGSAEMMOOL Pre-tect Sun Waterfull">
                                                <div class="cate-hover">
                                                    <h3>JJUNGSAEMMOOL Artist Brush Foundation</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">
                                                        ช่วยให้คุณเกลี่ยรองพื้นได้สวยงาม<br>และดูเป็นธรรมชาติ
                                                    </div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/15">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_15.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="JUNGSAEMMOOL Pre-tect Sun Waterfull">
                                                <div class="cate-hover">
                                                    <h3>Artist Brush Contour</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">ทำจากขนแปรงสังเคราะห์ที่นุ่มนวล<br>
                                                        ช่วยให้ใบหน้าดูมีมิติ 3D
                                                    </div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-18124 product type-product status-publish has-post-thumbnail product_cat-tools product_cat-227 shipping-taxable purchasable product-type-simple product-cat-tools instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/16">
                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_16.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="JUNGSAEMMOOL Pre-tect Sun Waterfull">
                                                <div class="cate-hover">
                                                    <h3>Artist Brush Hair Line</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">
                                                        แปรงหน้าตัดเฉียงที่ขนแปรงนุ่มแต่แน่น<br>
                                                        ดีไซน์เป็นพิเศษ</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>


                                    </div>
                                </div>
                         
    </div>
    <div id="base" class="tab-pane fade">
                                <div class="woocommerce columns-4">
                                    <div class="row row-products">
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/16">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_16.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Artist Contour Palette</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">เนื้อสัมผัสที่นุ่มละมุน<br>
                                                        ด้วยส่วนผสมจากแร่ธาตุช่วยให้แต่งแต้มผิวได้อย่างเรียบรื่น
                                                        และผิวไม่มีจุดด่างพร้อย</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/5">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_5.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Setting Glowing Base</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">
                                                        เบสที่ช่วยให้ผิวกระจ่างใสสุขภาพดี<br>
                                                        เหมาะสำหรับผิวแพ้ง่าย</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/6">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_6.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Setting Smoothing Base</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">
                                                        เมคอัพเบสที่ช่วยให้เครื่องสำอางติดทนนาน<br>
                                                        พร้อมควบคุมความมัน</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a
                                                href="..//shop/shopdetail.html?branduid=36&amp;xcode=003&amp;mcode=002&amp;scode=&amp;special=2&amp;GfDT=a2t3Ug%3D%3D"></a>
                                            <a class="product_link_wrap"
                                                href="..//shop/shopdetail.html?branduid=36&amp;xcode=003&amp;mcode=002&amp;scode=&amp;special=2&amp;GfDT=a2t3Ug%3D%3D">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_7.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Essential Star-cealer Foundation (Fair Light)</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">
                                                        ประสิทธิภาพที่ประสานกันระหว่างรองพื้น <br>
                                                        และคอนซีลเลอร์!</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/8">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_8.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Essential Skin Nuder Cushion (Fair Light)</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">การปกปิดอย่างสมบูรณ์แบบ <br>
                                                        ผิวโกลว์ ฉ่ำวาว ยาวนาน </div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/9">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_9.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Skin Setting Tone Manner Base SPF 30/ PA+++</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">เบสสำหรับผู้ชายออล อิน วัน<br>
                                                        ปกป้องแสงแดด</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/10">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_10.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Essential Skin Nuder Longwear Cushion (Fair Light)
                                                    </h3>
                                                    <strong class="price">

                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">
                                                        รองพื้นคูชชั่นที่ให้เนื้อสัมผัสนุ่มละมุน<br>
                                                        ติดทนนาน </div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/11">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_11.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Cushion-cealer (Fair Light)</h3>
                                                    <strong class="price">

                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">คูชชั่นที่เน้นการปกปิด <br>
                                                        พร้อมด้วย SPF 50+ / PA+++</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                    </div>
                                </div>
                         
    </div>
    <div id="lip" class="tab-pane fade">
                                <div class="woocommerce columns-4">
                                    <div class="row row-products">
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/3">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_3.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL High Tinted Lip Lacquer( Rose Cosset)</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">ลิปแลคเกอร์ที่ให้เม็ดสีสดชัด<br>
                                                        เพิ่มความชุ่มชื้นให้ริมฝีปาก</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/4">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_4.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Lip-pression #JUST RED</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">สีชวนหลงใหล<br>
                                                        น่าดึงดูด</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>

                                    </div>
                                </div>
                          
    </div>
    <div id="eye" class="tab-pane fade">
                                <div class="woocommerce columns-4">
                                    <div class="row row-products">
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/12">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_12.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Refining Color-bony Brow (Light-bony)</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">ดุจดั่งปลายดินสอของศิลปิน Refining
                                                        Color Bony Brow<br>
                                                        ช่วยให้คุณเขียนคิ้วได้สวยเฉียบดั่งใจ</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>

                                    </div>
                                </div>
                         
    </div>
    <div id="skincare" class="tab-pane fade">
                                <div class="woocommerce columns-4">
                                    <div class="row row-products">
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/1">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_1.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Pre-tect Sun Waterfull</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">SPF50+/ PA++++<br>
                                                        เนื้อบางเบา</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>

                                    </div>
                                </div>
                        
    </div>
    <div id="tools" class="tab-pane fade">
                                <div class="woocommerce columns-4">
                                    <div class="row row-products">
                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/13">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_13.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Artist Make-up Blending Tool</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">สะดวกสบาย<br>
                                                        และหลากหลายประโยชน์ </div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>

                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/14">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_14.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>JUNGSAEMMOOL Artist Brush Foundation</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">
                                                        ช่วยให้คุณเกลี่ยรองพื้นได้สวยงาม<br>และดูเป็นธรรมชาติ
                                                    </div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>

                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/15">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_15.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>Artist Brush Contour</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">ทำจากขนแปรงสังเคราะห์ที่นุ่มนวล<br>
                                                        ช่วยให้ใบหน้าดูมีมิติ 3D
                                                    </div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>

                                        <div
                                            class="col-lg-3 col-md-3 col-sm-4 col-xs-12 post-24994 product type-product status-publish has-post-thumbnail product_cat-base-make-up product_cat-227 shipping-taxable purchasable product-type-variable product-cat-base-make-up has-children instock">
                                            <a href=""></a>
                                            <a class="product_link_wrap" href="/product/detail/view/16">

                                                <img width="600" height="600"
                                                    src="{!!asset('/jsmbeauty/src/Product/product_16.jpg')!!}"
                                                    class="MS_prod_img_l attachment-shop_catalog size-shop_catalog wp-post-image"
                                                    alt="600">
                                                <div class="cate-hover">
                                                    <h3>Artist Brush Hair Line</h3>
                                                    <strong class="price">
                                                        <span class="amount">0 ฿ </span>
                                                    </strong>
                                                    <div class="small post_excerpt">
                                                        แปรงหน้าตัดเฉียงที่ขนแปรงนุ่มแต่แน่น<br>
                                                        ดีไซน์เป็นพิเศษ</div>
                                                </div>
                                            </a>
                                            <hr class="clear">
                                        </div>

                                    </div>
                                </div>
                            </div>
    </div>
  </div>

                    
                        </div>
                    </div>
                </div><!-- .wpb_wrapper -->
            </div><!-- .vc_column-inner -->
        </div><!-- .wpb_column vc_column_container vc_col-sm-12 -->
    </div>
</div>

<div class="vc_row wpb_row vc_row-fluid vc_custom_1464829672746 vc_row-has-fill">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner vc_custom_1446006821477">
            <div class="wpb_wrapper">
                <div class="wpb_single_image wpb_content_element vc_align_center">
                    <figure class="wpb_wrapper vc_figure">
                        <a href="" target="_self" class="vc_single_image-wrapper vc_box_border_grey">
                            <img width="1602" height="836" src="{!!asset('jsmbeauty/src/FINDYOURBEAUTY.jpg')!!}"
                                class="vc_single_image-img attachment-full" alt="FINDYOURBEAUTY" /></a>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<script>



</script>