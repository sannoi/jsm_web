@extends('layouts.template.frontend')

<style type="text/css">
#ju-container .ju-page-title {
    margin-top: 179px;
}
</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM Artist Tip </title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <link type="text/css" rel="stylesheet" href="/shopimages/jsmbeauty/template/work/21081/common.css?r=1487237021" />
    <link type="text/css" rel="stylesheet" href="/images/d3/m_01/css/font-awesome.min.css" />
    <link rel='stylesheet' id='jung_theme_bootstrap_css-css' href='/design/jsmbeauty/css/bootstrap.css' type='text/css'
        media='all' />
    <link rel='stylesheet' id='jung_theme_base_css-css' href='/design/jsmbeauty/css/bootstrap-theme.css' type='text/css'
        media='all' />
    <link rel='stylesheet' id='js_composer_front-css' href='/design/jsmbeauty/css/js_composer.min.css' type='text/css'
        media='all' />
    <link rel='stylesheet' id='sb_instagram_styles-css' href='/design/jsmbeauty/css/sb-instagram.min.css?ver=1.4.6'
        type='text/css' media='all' />
    <link rel='stylesheet' id='sb_instagram_icons-css'
        href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css' type='text/css'
        media='all' />
    <link rel='stylesheet' id='vc_google_fonts_abril_fatfaceregular-css'
        href='//fonts.googleapis.com/css?family=Abril+Fatface%3Aregular&ver=4.5.3' type='text/css' media='all' />

    <link rel="stylesheet" type="text/css" href="/design/jsmbeauty/css/woocommerce.css" media="all" />

    <link type="text/css" rel="stylesheet"
        href="/shopimages/jsmbeauty/template/work/21081/board_list.jsmbeauty_image2.css?t=201707271722" />

    <link type="text/css" rel="stylesheet"
        href="/shopimages/jsmbeauty/template/work/21081/header.1.css?t=201908011343" />
</head>

@section('content')

<script type="text/javascript" src="//wcs.naver.net/wcslog.js"></script>
<script type="text/javascript">
function setCookieCpa(cookie_name, cookie_value, expire_date) {

    var cookieurl = '.jsmbeauty.com';
    var today = new Date();
    var expire = new Date();
    expire.setTime(today.getTime() + 3600000 * 24 * expire_date);
    cookies = cookie_name + '=' + cookie_value + '; path=/;';
    cookies += (cookieurl) ? ' DOMAIN=' + cookieurl + '; ' : '';
    if (expire_date != 0) cookies += 'expires=' + expire.toGMTString();
    document.cookie = cookies;
}

function delCookieCpa(cookie_name) {

    var cookieurl = '.jsmbeauty.com';
    var _today = new Date();
    var value = '';

    _today.setDate(_today.getDate() - 1);
    cookies = cookie_name + '=' + value + '; path=/;';
    cookies += (cookieurl) ? ' DOMAIN=' + cookieurl + '; ' : '';
    document.cookie = cookie_name + "=" + value + "; expires=" + _today.toGMTString() + "; " + cookies;
}

if (typeof wcs == 'object') {

    if (!wcs_add) var wcs_add = {};
    wcs_add["wa"] = "s_eafdb778b82";
    wcs.checkoutWhitelist = ["jsmbeauty.com", "www.jsmbeauty.com"];
    wcs.setReferer("https://www.jsmbeauty.com/m/board.html?code=jsmbeauty_image2&page=2&board_cate=5");


    wcs.inflow("jsmbeauty.com");

    if (wcs.isCPA) {
        setCookieCpa('isCPA', 'Y', 1);
    }

}
</script>

<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
function getCookiefss(name) {
    lims = document.cookie;
    var index = lims.indexOf(name + "=");
    if (index == -1) {
        return null;
    }
    index = lims.indexOf("=", index) + 1; // first character
    var endstr = lims.indexOf(';', index);
    if (endstr == -1) {
        endstr = lims.length; // last character
    }
    return unescape(lims.substring(index, endstr));
}
</script>
<script type="text/javascript">
var MOBILE_USE = '1';
</script>
<script type="text/javascript">
var is_powerpack = 0;
var StringBuffer = function() {
    this.buffer = new Array();
};

StringBuffer.prototype.append = function(str) {
    this.buffer[this.buffer.length] = str;
};

StringBuffer.prototype.toString = function() {
    return this.buffer.join("");
};

function checkSession() {
    if (navigator.userAgent.match(/(iPad|iPhone|iPod)/g)) {
        return 'N';
    } else {
        alert('N');
    }
}

function checkPowerappLoginState() {
    var checkreq = {
        'login': 'N',
        'login_id_key': ''
    };
    jQuery.ajax({
        url: '/m/userinfo_check.ajax.html',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: {
            action_type: "powerapp_login_chk"
        },
        success: function(req) {
            checkreq.login = req.login;
            checkreq.login_id_key = req.login_id_key;
        }
    });
    if (navigator.userAgent.match(/(iPad|iPhone|iPod)/g)) {
        return checkreq;
    } else {
        window.android.checkPowerappLoginState(checkreq.login, checkreq.login_id_key);
        return;
    }
}
</script>
<script type="text/javascript" src="/js/prototype.js"></script>

<script type="text/javascript">
function sns_login(us_type) {
    var action_url = '';
    var _chk = 'Y';
    switch (us_type) {
        case 'facebook':
        case 'naver':
        case 'kakao':
        case 'twitter':
            action_url = '/list/API/login_' + us_type + '.html';
            break;
    }
    if (action_url != '') {

        if (_chk == 'Y') {
            document.login_form.id.value = '';
            document.login_form.passwd.value = '';
            document.login_form.msecure_key.value = '';
            document.login_form.target = '_self';
            document.login_form.action = action_url;
            document.login_form.submit();
        }
    }
}

var shop_name = '정샘물뷰티';
</script>


<div id="ju-container">
    <div id="ju-content" class="container">
        <div class="ju-page-title">
            <h1 class="entry-title text-gotham text-center">Artist Tip</h1>
            <div class="text-center text-gotham">
                <p>เราแนะนำเคล็ดลับความงามให้คุณ.</p>
            </div>
            <style>
            .title {
                margin-bottom: 65px;
            }

            .tabs-wrap {
                border: 0;
                border-top: 0px;
                padding-top: 15px;
            }

            .view_none img {
                display: none;
            }
            </style>
        </div>
        <!-- title -->
        <div id="bbsData">
            <div class="page-body">
                <div class="bbs-table-list tabs-wrap">
                    <table summary="제목, 작성일, 조회수, 동영상">
                        <caption class="displaynone">คลังภาพ</caption>
                        <colgroup>
                            <col width="*" />
                            <col width="110" />
                            <col width="60" />
                        </colgroup>
                        <tbody>
                        </tbody>
                    </table>
                    <div class="video-list">
                        <table summary="제목, 작성일, 조회수, 동영상">
                            <colgroup>
                                <col width="285px" />
                                <col width="285px" />
                                <col width="285px" />
                                <col width="285px" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="box">
                                            <a href="/Detail/Artis"
                                                class="video_link">
                                                <div class="video_txt">
                                                    <h6 class="letters">[Artist tip]
                                                        แก้ปัญหารูขุมขนและลดหน้ามันแม้ในอากาศที่ร้อน</h6>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024"
                                                        src="/jsmbeauty/src/ArtistTip/562_314_you_tube_02_2.jpg"
                                                        class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="#"
                                                class="video_link">
                                                <div class="video_txt">
                                                    <h6 class="letters">[Artist tip] การแต่งหน้าสำหรับริมฝีปากสองเฉด
                                                    </h6>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024"
                                                        src="/jsmbeauty/src/ArtistTip/562_314_you_tube_01_3.jpg"
                                                        class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="#"
                                                class="video_link">
                                                <div class="video_txt">
                                                    <h6 class="letters">[Artist tip] การแต่งหน้า 'สีแดง' ของคุณ</h6>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024"
                                                        src="/jsmbeauty/src/ArtistTip/02_6.jpg"
                                                        class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="#"
                                                class="video_link">
                                                <div class="video_txt">
                                                    <h6 class="letters">[Artist tip]
                                                        วิธีสร้างกิจวัตรการดูแลผิวที่สมบูรณ์แบบ</h6>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024"
                                                        src="/jsmbeauty/src/ArtistTip/01_7.jpg"
                                                        class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="box">
                                            <a href="#"
                                                class="video_link">
                                                <div class="video_txt">
                                                    <h6 class="letters">[Artist tip] วิธีการทำผม</h6>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024"
                                                        src="/jsmbeauty/src/ArtistTip/02_5.jpg"
                                                        class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="#"
                                                class="video_link">
                                                <div class="video_txt">
                                                    <h6 class="letters">[Artist tip] Rozy Makeup</h6>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024"
                                                        src="/jsmbeauty/src/ArtistTip/562_314_you_tube_6.jpg"
                                                        class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="#"
                                                class="video_link">
                                                <div class="video_txt">
                                                    <h6 class="letters">[Artist tip] การแต่งหน้าง่ายๆ สำหรับผู้ชาย</h6>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024"
                                                        src="/jsmbeauty/src/ArtistTip/01_6.jpg"
                                                        class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="#"
                                                class="video_link">
                                                <div class="video_txt">
                                                    <h6 class="letters">[Artist tip] การแต่งหน้าในฤดูหนาว</h6>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024"
                                                        src="/jsmbeauty/src/ArtistTip/562_314_you_tube_3.jpg"
                                                        class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="box">
                                            <a href="#"
                                                class="video_link">
                                                <div class="video_txt">
                                                    <h6 class="letters">[Artist tip] การแต่งหน้าในวันหยุด</h6>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024"
                                                        src="/jsmbeauty/src/ArtistTip/04_3.jpg"
                                                        class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="#"
                                                class="video_link">
                                                <div class="video_txt">
                                                    <h6 class="letters">[Artist tip] แต่งหน้าลุคเสือดาว</h6>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024"
                                                        src="/jsmbeauty/src/ArtistTip/03_3.jpg"
                                                        class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="#"
                                                class="video_link">
                                                <div class="video_txt">
                                                    <h6 class="letters">[Artist tip] การใช้งาน Lacquer</h6>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024"
                                                        src="/jsmbeauty/src/ArtistTip/02_4.jpg"
                                                        class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="box">
                                            <a href="#"
                                                class="video_link">
                                                <div class="video_txt">
                                                    <h6 class="letters">[Artist tip] ลุคฤดูใบไม้ร่วงคลาสสิก</h6>
                                                    <p class="letters"></p>
                                                </div>
                                                <div class="video-thumbnail">
                                                    <img width="683" height="1024"
                                                        src="/jsmbeauty/src/ArtistTip/01_5.jpg"
                                                        class="attachment-large size-large wp-post-image" alt="4">
                                                </div>
                                            </a>
                                            <hr class="clear sm">
                                        </div>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!--.bbs-table-list-->
                <div class="bbs-sch displaynone">
                    <form action="board.html" name="form1">
                        <input type=hidden name=s_id value="">
                        <input type=hidden name=code value="jsmbeauty_image2">
                        <input type=hidden name=page value=1>
                        <input type=hidden name=type value=s>
                        <input type=hidden name=board_cate value="5">
                        <input type=hidden name="review_type" value="" />
                        <fieldset>
                            <legend class="displaynone">ค้นหากระดานข่าว</legend>
                            <label>
                                <select name="search_type" class="brd-st">
                                    <option value="">การเลือก</option>
                                    <option value="hname">ชื่อ</option>
                                    <option value="subject">เรื่อง</option>
                                    <option value="content">สารบัญ</option>

                                </select> </label>
                            <span class="key-wrap">
                                <input type='text' name='stext' value='' class="MS_input_txt" /> <a
                                    href="javascript:document.form1.submit();" class="btn btn-gray"><i
                                        class="fa fa-search"></i> การแก้ไข</a>
                            </span>
                        </fieldset>
                    </form>
                </div><!-- .bbs-sch -->
                <dl class="bbs-link bbs-link-btm displaynone">
                    <dd>
                        <a class="write"
                            href="/board/board.html?code=jsmbeauty_image2&page=1&board_cate=5&type=i">การเขียน</a>
                    </dd>
                </dl>
                <nav class="text-center">
                    <ul class="pagination">
                        <li><span class="page-numbers current">1</span></li>
                        <li><a href="/artist/tip2" class="page-numbers">2</a></li>
                        <!-- <li><a href="/artist/tip03" class="page-numbers">3</a></li>
                        <li><a href="/artist/tip04" class="page-numbers">4</a></li>
                        <li><a href="/artist/tip05" class="page-numbers">5</a></li>
                        <li><a href="/artist/tip06" class="page-numbers">6</a></li> -->
                        <!-- <li><a href="/artist/tip"
                                class="page-numbers">7</a></li>
                        <li><a href="/artist/tip"
                                class="page-numbers">8</a></li> -->
                        <!-- <li class="last"><a
                                href="/board/board.html?code=jsmbeauty_image2&page=8&board_cate=5#board_list_target"
                                class="last page-numbers end"><i class="fa fa-forward"></i></a></li> -->
                    </ul>
                </nav>
            </div>
        </div><!-- .page-body -->
    </div><!-- #bbsData -->
</div><!-- #contentWrap -->


<link type="text/css" rel="stylesheet" href="/shopimages/jsmbeauty/template/work/21076/footer.1.css?t=201904041110" />
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>무제 문서</title>
</head>

<body>

    </div><!-- #wrap -->

    <div id="mask"></div>

    <script type='text/javascript' src='/design/jsmbeauty/js/masterslider.min.js'></script>
    <script type='text/javascript' src='/design/jsmbeauty/js/bootstrap.min.js'></script>
    <script type='text/javascript' src='/design/jsmbeauty/js/script.js'></script>
    <script type='text/javascript' src='/design/jsmbeauty/js/wp-embed.min.js'></script>
    <script type='text/javascript' src='/design/jsmbeauty/js/core.min.js'></script>
    <script type='text/javascript' src='/design/jsmbeauty/js/js_composer_front.min.js'></script>
    <script type='text/javascript' src='/design/jsmbeauty/js/widget.min.js'></script>
    <script type='text/javascript' src='/design/jsmbeauty/js/tabs.min.js'></script>





    <!-- AceCounter Log Gathering Script V.7.5.2013010701 -->
    <script language='javascript'>
    var _AceGID = (function() {
        var Inf = ['gtp9.acecounter.com', '8080', 'BH6A41239969548', 'AW', '0', 'NaPm,Ncisy', 'ALL', '0'];
        var _CI = (!_AceGID) ? [] : _AceGID.val;
        var _N = 0;
        var _T = new Image(0, 0);
        if (_CI.join('.').indexOf(Inf[3]) < 0) {
            _T.src = (location.protocol == "https:" ? "https://" + Inf[0] : "http://" + Inf[0] + ":" + Inf[1]) +
                '/?cookie';
            _CI.push(Inf);
            _N = _CI.length;
        }
        return {
            o: _N,
            val: _CI
        };
    })();
    var _AceCounter = (function() {
        var G = _AceGID;
        if (G.o != 0) {
            var _A = G.val[G.o - 1];
            var _G = (_A[0]).substr(0, _A[0].indexOf('.'));
            var _C = (_A[7] != '0') ? (_A[2]) : _A[3];
            var _U = (_A[5]).replace(/\,/g, '_');
            var _S = ((['<scr', 'ipt', 'type="text/javascr', 'ipt"></scr', 'ipt>']).join('')).replace('tt',
                't src="' + location.protocol + '//cr.acecounter.com/Web/AceCounter_' + _C + '.js?gc=' + _A[
                    2] + '&py=' + _A[4] + '&gd=' + _G + '&gp=' + _A[1] + '&up=' + _U + '&rd=' + (new Date()
                    .getTime()) + '" t');
            document.writeln(_S);
            return _S;
        }
    })();
    </script>
    <noscript><img src='http://gtp9.acecounter.com:8080/?uid=BH6A41239969548&je=n&' border='0' width='0' height='0'
            alt=''></noscript>
    <!-- AceCounter Log Gathering Script End -->

    <script type="text/javascript">
    var roosevelt_params = {
        retargeting_id: 'kIZSNQo1SWjOI-LsGsV9PQ00',
        tag_label: 'JRsZsYJbTsqD0jhMTcMJOg'
    };
    </script>
    <script type="text/javascript" src="//adimg.daumcdn.net/rt/roosevelt.js" async></script>

</body>

</html>



<script type="text/javascript" src="/shopimages/jsmbeauty/template/work/21076/footer.1.js?t=201904041110"></script>

<iframe id="loginiframe" name="loginiframe" style="display: none;" frameborder="no" scrolling="no"></iframe>

<div id="basketpage" name="basketpage" style="position: absolute; visibility: hidden;"></div>

<script type="text/javascript">
(function($) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/html/user_basket_quantity.html',
        data: {
            'IS_UNIFY_OPT': "true"
        },
        success: function(res) {
            var _user_basket_quantity = res.user_basket_quantity || 0;
            $('.user_basket_quantity').html(_user_basket_quantity);
        },
        error: function(error) {
            var _user_basket_quantity = 0;
            $('.user_basket_quantity').html(_user_basket_quantity);
        }
    });
})(jQuery);



function search_submit() {
    var oj = document.search;
    if (oj.getAttribute('search') != 'null') {
        var reg = /\s{2}/g;
        oj.search.value = oj.search.value.replace(reg, '');
        oj.submit();
    }
}

function topnotice(temp, temp2) {
    window.open("/html/notice.html?date=" + temp + "&db=" + temp2, "", "width=450,height=450,scrollbars=yes");
}

function notice() {
    window.open("/html/notice.html?mode=list", "", "width=450,height=450,scrollbars=yes");
}

function view_join_terms() {
    window.open('/html/join_terms.html', 'join_terms', 'height=570,width=590,scrollbars=yes');
}

function bottom_privacy() {
    window.open('/html/privacy.html', 'privacy', 'height=570,width=590,scrollbars=yes');
}

if (typeof wcs_do == 'function') {
    wcs_do();
}

function upalert() {
    alert('파일첨부 버튼을 클릭하세요');
    document.form1.subject.focus();
}

function clear_content() {
    return;
}

function change(idx) {
    onoff = new Array(document.form1.shname, document.form1.ssubject, document.form1.scontent, document.form1.sbrand,
        document.form1.sgid);

    for (var i = 0; i < onoff.length; i++) {
        if (typeof onoff[i] != 'undefined') {
            if ((i + 1) == idx) {
                onoff[i].checked = true;
            } else {
                onoff[i].checked = false;
            }
        }
    }
}
</script>
<script type="text/javascript">
if (typeof getCookie == 'undefined') {
    function getCookie(cookie_name) {
        var cookie = document.cookie;
        if (cookie.length > 0) {
            start_pos = cookie.indexOf(cookie_name);
            if (start_pos != -1) {
                start_pos += cookie_name.length;
                end_pos = cookie.indexOf(';', start_pos);
                if (end_pos == -1) {
                    end_pos = cookie.length;
                }
                return unescape(cookie.substring(start_pos + 1, end_pos));
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
if (typeof setCookie == 'undefined') {
    function setCookie(cookie_name, cookie_value, expire_date, domain) {
        var today = new Date();
        var expire = new Date();
        expire.setTime(today.getTime() + 3600000 * 24 * expire_date);
        cookies = cookie_name + '=' + escape(cookie_value) + '; path=/;';

        if (domain != undefined) {
            cookies += 'domain=' + domain + ';';
        } else if (document.domain.match('www.') != null) {
            cookies += 'domain=' + document.domain.substr(3) + ';';
        }
        if (expire_date != 0) cookies += 'expires=' + expire.toGMTString();
        document.cookie = cookies;
    }
}

function setMakeshopLogUniqueId() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
}

if (getCookie('MakeshopLogUniqueId')) {
    var MakeshopLogUniqueId = getCookie('MakeshopLogUniqueId');
} else {
    var MakeshopLogUniqueId = setMakeshopLogUniqueId();
    setCookie('MakeshopLogUniqueId', MakeshopLogUniqueId);
}

function MSLOG_loadJavascript(url) {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    var loaded = false;
    script.onreadystatechange = function() {
        if (this.readyState == 'loaded' || this.readyState == 'complete') {
            if (loaded) {
                return;
            }
            loaded = true;
        }
    }
    script.src = url;
    head.appendChild(script);
}
var MSLOG_charset = "euc-kr";
var MSLOG_server = document.location.protocol + "//log20.makeshop.co.kr";
var MSLOG_code = "jsmbeauty";
var MSLOG_var =
    "V1ZSdmVrOXVkSHBQYWtWNlQybEtkbU50VW14amJEbHlXbGhzTTJJelNtdEphblJQVHpOTk5rNXFiMmxqYlZadFpGaEtjMGxxZEU5UE0wMDJUbFJ2YVdSSE9XdFpXR3RwVHpOTk5rMUViMmxKYW5RNQ==";

//파워앱에서만 사용
try {
    var LOGAPP_var = "";
    var LOGAPP_is = "N";
    if (LOGAPP_is == "Y" && LOGAPP_var != "") {
        var varUA = navigator.userAgent.toLowerCase(); //userAgent 값 얻기
        if (varUA.match('android') != null) {
            //안드로이드 일때 처리
            window.android.basket_call(LOGAPP_var);
        } else if (varUA.indexOf("iphone") > -1 || varUA.indexOf("ipad") > -1 || varUA.indexOf("ipod") > -1) {
            //IOS 일때 처리
            var messageToPost = {
                LOGAPP_var: LOGAPP_var
            };
            window.webkit.messageHandlers.basket_call.postMessage(messageToPost);
        } else {
            //아이폰, 안드로이드 외 처리
        }
    }
} catch (e) {}
//파워앱에서만 사용 END

if (document.charset) MSLOG_charset = document.charset.toLowerCase();
if (document.characterSet) MSLOG_charset = document.characterSet.toLowerCase(); //firefox;
MSLOG_loadJavascript(MSLOG_server + "/js/mslog.js?r=" + Math.random());
</script>
<script type="text/javascript">
var pagekin_el = document.getElementsByTagName("div");
var pagekin_id = [];
for (var i = 0; i < pagekin_el.length; i++) {
    if (pagekin_el[i].className.substring(0, 4) == "PKMW") {
        pagekin_id.push(pagekin_el[i].className);
    }
}
if (pagekin_id.length > 0) {
    var script = document.createElement('script');
    script.src = '//image.makeshop.co.kr/pagekin/widget/makeshop.d.js??20190815';
    script.charset = 'utf-8';
    document.body.appendChild(script);
}
</script>
<!--script type="text/javascript" src="//www.pagekin.com/widget/makeshop.js" charset="utf-8"></script-->
<script type="text/javascript" src="/js/cookie.js"></script>
<script type="text/javascript">
function __mk_open(url, name, option) {
    window.open(url, name, option);
    //return false;
}

function action_invalidity() {
    return false;
}
</script>
<script type="text/javascript"></script>
<script type="text/javascript"></script>
<script>
function getInternetExplorerVersion() {
    var rv = -1;
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}

function showcherrypickerWindow(height, mode, db) {
    cherrypicker_width = document.body.clientWidth;
    var isIe = /*@cc_on!@*/ false;
    if (isIe) {
        cherrypicker_width = parseInt(cherrypicker_width + 18);
    }
    setCookie('cherrypicker_view', 'on', 0);


    document.getElementById('cherrypicker_layer').style.display = "block";
    document.getElementById('cherrypicker_layer').innerHTML = Createflash_return(cherrypicker_width, '',
        '/flashskin/CherryPicker.swf?initial_xml=/shopimages/jsmbeauty/cherrypicker_initial.xml%3Fv=1565844298&product_xml=/shopimages/jsmbeauty/%3Fv=1565844298',
        'cherrypicker_flash', '');
}

function load_cherrypicker() {
    cherrypicker_check = true;

    if (!document.getElementById('cherrypicker_layer')) {
        return;
    }


}
</script>
<script>
var inputs = document.getElementsByTagName("input");
for (x = 0; x <= inputs.length; x++) {
    if (inputs[x]) {
        myname = inputs[x].getAttribute("name");
        if (myname == "ssl") {
            inputs[x].checked = 'checked';
        }
    }
}
(function($) {
    $(document).ready(function() {
        jQuery(':checkbox[name=ssl]').click(function() {
            this.checked = true;
        });
    });
})(jQuery);
</script>
<script type="text/javascript" src="/template_common/shop/modern_simple/common.js?r=1391500980"></script>




@endsection