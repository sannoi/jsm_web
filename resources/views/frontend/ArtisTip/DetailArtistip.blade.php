@extends('layouts.template.frontend')

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM Artist Tip </title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <link type="text/css" rel="stylesheet" href="/shopimages/jsmbeauty/template/work/21081/common.css?r=1487237021" />
    <link type="text/css" rel="stylesheet" href="/images/d3/m_01/css/font-awesome.min.css" />
    <link rel='stylesheet' id='jung_theme_bootstrap_css-css' href='/design/jsmbeauty/css/bootstrap.css' type='text/css'
        media='all' />
    <link rel='stylesheet' id='jung_theme_base_css-css' href='/design/jsmbeauty/css/bootstrap-theme.css' type='text/css'
        media='all' />
    <link rel='stylesheet' id='js_composer_front-css' href='/design/jsmbeauty/css/js_composer.min.css' type='text/css'
        media='all' />
    <link rel='stylesheet' id='sb_instagram_styles-css' href='/design/jsmbeauty/css/sb-instagram.min.css?ver=1.4.6'
        type='text/css' media='all' />
    <link rel='stylesheet' id='sb_instagram_icons-css'
        href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css' type='text/css'
        media='all' />
    <link rel='stylesheet' id='vc_google_fonts_abril_fatfaceregular-css'
        href='//fonts.googleapis.com/css?family=Abril+Fatface%3Aregular&ver=4.5.3' type='text/css' media='all' />

    <link rel="stylesheet" type="text/css" href="/design/jsmbeauty/css/woocommerce.css" media="all" />

    <link type="text/css" rel="stylesheet"
        href="/shopimages/jsmbeauty/template/work/21081/board_list.jsmbeauty_image2.css?t=201707271722" />

    <link type="text/css" rel="stylesheet"
        href="/shopimages/jsmbeauty/template/work/21081/header.1.css?t=201908011343" />
</head>

@section('content')

<script type="text/javascript" src="/shopimages/jsmbeauty/template/work/21076/header.1.js?t=201906251654"></script>

<div id="ju-container">
    <div id="ju-content" class="container">
        <div class="ju-page-title">
            <div class="title">
                <h1>ARTIST TIP</h1>
                <p>เราแนะนำเคล็ดลับความงามให้คุณ.</p>
                <style>
                .title {
                    margin-bottom: 65px;
                }

                .tabs-wrap {
                    border: 0;
                    border-top: 0px;
                    padding-top: 15px;
                }

                .view_none img {
                    display: none;
                }
                </style>
            </div>
            <hr class="clear">
            <h3 class="letters">[Artist tip] แก้ปัญหารูขุมขนและลดหน้ามันแม้ในอากาศที่ร้อน.<span
                    class="label label-danger text-white"></span></h3>
            <main id="review-board-type">
                <dl class="rbInfo displaynone">
                    <dt>[Artist tip] แก้ปัญหารูขุมขนและลดหน้ามันแม้ในอากาศที่ร้อน</dt>
                    <!-- <dd>
                        <span class="rb_name"><img src="/board/images/neo_adminimg.gif" border="0"
                                align="absmiddle"></span> <span class="split">|</span> 2019-07-04 <span
                            class="split">|</span> 조회수 213 </dd> -->
                </dl>
                <div class="rbContent">
                    <div class="rb_thumbs">
                        <img src="/jsmbeauty/src/ArtistTip/562_314_you_tube_02_2.jpg" class="response100"
                            style="width: 100%;">
                    </div>
                    <div class="content">
                        <table style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td>
                                        <div id="MS_WritenBySEB1">
                                            <div align="center" style="text-align: center;"><iframe width="100%"
                                                    height="550" src="https://www.youtube.com/embed/6YzTWfh0gKw"
                                                    frameborder="0" lowfullscreen="" al=""></iframe></div>
                                        </div>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="rb_icons">
                    </div>
                </div>
                <!-- 댓글쓰기 -->
                <!-- //댓글쓰기 -->
                <hr>
                <div class="write-btn">
                    <!-- <div class="small-btns displaynone">
                        <a href="/m/board.html?code=jsmbeauty_image2&amp;page=1&amp;board_cate=5&amp;num1=999602&amp;num2=00000&amp;type=q&amp;type2=u&amp;s_id=&amp;stext=&amp;ssubject=&amp;shname=&amp;scontent=&amp;sbrand=&amp;sgid=&amp;datekey=&amp;branduid="
                            class="btn_Grey">수정</a> <a
                            href="/m/board.html?code=jsmbeauty_image2&amp;page=1&amp;board_cate=5&amp;num1=999602&amp;num2=00000&amp;type=q&amp;type2=d&amp;s_id=&amp;stext=&amp;ssubject=&amp;shname=&amp;scontent=&amp;sbrand=&amp;sgid=&amp;datekey=&amp;branduid="
                            class="btn_Grey">삭제</a> </div> -->
                    <a href="/artist/tip" class="btn btn-primary btn-lg btn-block letters">รายการ</a>
                </div>

            </main>

        </div>
    </div>


    @endsection