@extends('layouts.template.frontend')

<style type="text/css">
#ju-container .ju-page-title {
    margin-top: 179px;
}
</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM ARTIST </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>

@section('content')

<div id="ju-container">
    <div id="ju-content" class="container">
        <div class="ju-page-title">
            <h1 class="entry-title">FAQ</h1>
            <div class="f_title_div">
                กรุณาเลือกสิ่งที่คุณต้องการตามประเภท</div>
        </div>
        <!-- main -->
        <main id="faqWrap">
            <!-- 검색영역 -->
            <div class="search-wrap well">
                <div class="item-search input-group">
                    <label class="displaynone"> <select class="MS_input_select select-category custom-blue"
                            id="search-category">
                            <option value="">전체검색</option>
                            <option value="1">배송관련</option>
                            <option value="2">주문,결제</option>
                            <option value="3">취소,환불,교환</option>
                            <option value="4">쿠폰,포인트</option>
                        </select></label>
                    <span class="input-group-addon letters" style="border-right:0;"><i class="fa fa-search"></i></span>
                    <input id='faqSearch' class="MS_input_txt form-control" onKeyPress='javascript:faqEnter(event);'
                        type='text' value='' /> <span class="input-group-btn"><a href="javascript:faqSearch('keyword')"
                            class="btn btn-default letters">ค้นหา</a></span>
                </div>
            </div>
            <!-- //검색영역 -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="">ทั้งหมด</a></li>
                <li><a href="">การจัดส่ง</a></li>
                <li><a href="">ใบสั่งซื้อ</a></li>
                <li><a href="">คืนเงินให้</a></li>
                <li><a href="">คูปอง</a></li>
            </ul><!-- .faq-category-->
            <div class="faq-list">
                <table id="faqTable" class="panel">
                    <tbody>
                        <tr uid="22">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd><br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                        </dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="16">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd>
                                            <p>&nbsp;</p>


                                            <strong> </strong>
                                            <p> </p>


                                            <p>&nbsp;</p>
                                        </dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="15">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd><strong> </strong>

                                            <p>&nbsp;</p>


                                            <strong> </strong>
                                            <p>&nbsp;</p>

                                            <p>&nbsp;</p>
                                        </dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="21">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd>
                                        </dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="20">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd></dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="19">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd>
                                            <p>&nbsp;</p>

                                            <strong></strong>
                                            <strong></strong>


                                            <p>&nbsp;</p>

                                            <strong></strong>
                                            <strong></strong>
                                        </dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="18">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd>

                                        </dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="17">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd></dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="14">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd></dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="13">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd></dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="12">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd></dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="11">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd><strong></strong> <strong></strong>
                                            <strong></strong> <strong></strong></dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="10">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd>
                                        </dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="9">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd> </dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="8">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd>

                                        </dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="7">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd></dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="6">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd> </dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="5">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd>



                                        </dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="4">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd></dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="3">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd></dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="2">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd></dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                        <tr uid="1">
                            <td class="col-sm-2">
                                <div class="tb-center"><span class="label label-default"></span></div>
                            </td>
                            <td class="col-sm-10">
                                <div class="tb-left bold"></div>
                            </td>
                        </tr>
                        <tr class="slide-hide">
                            <td colspan="2">
                                <div class="tb-slide">
                                    <dl class="adv">
                                        <dt></dt>
                                        <dd></dd>
                                    </dl>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="paging">
                <a href="javascript:return false;" class="now">1</a>
            </div>
            </form>
        </main>
        <!-- //main -->
    </div>
    <!-- //contents -->
</div>


@endsection