@extends('layouts.template.frontend')

<style type="text/css">
#ju-container .ju-page-title {
    margin-top: 179px;
}
</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM ARTIST </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>

@section('content')

<div id="ju-container">
    <div id="ju-content" class="container">
        <div class="ju-page-title">
            <h1 class="entry-title">Q & A</h1>
            
        </div>
        <!-- main -->
        <main id="review-board-list">

            <div style="padding: 3%;">
                <form style="text-align: right;">
                    <input type="radio" name="Name" value="Name"><label style="padding-right: 2%;">Name</label> 
                    <input type="radio" name="Title" value="Title"><label style="padding-right: 2%;">Title</label>  
                    <input type="radio" name="Content" value="Content"><label style="padding-right: 2%;">Content</label>
                    <span>
                        <input type="text" style="" style="margin: 3%;">
                        <img
                            src="//www.jsmbeauty.us/storage/jsmbeautyEN/www/frontstore/default_5/EN/Frontend/btn/btn_bbs_sch.gif">
                    </span>

                </form>
            </div>
            <div style="padding-bottom: 3%;">
                <!-- <table>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col"></th>
                    </tr>
                    <tr>
                        <td>January</td>
                    </tr>
                </table> -->
                <div class="bbs-table-list">
                    <table style="width: 100%;">
                        <colgroup>
                            <col width="50px">
                            <col width="130px">
                            <col width="*">
                            <col width="110px">
                            <col width="110px">
                            <col width="50px">
                        </colgroup>
                        <thead>
                            <tr>
                                <th scope="col">
                                    <div class="tb-center">No.</div>
                                </th>
                                <th scope="col"></th>

                                <th scope="col">
                                    <div class="tb-center">Content</div>
                                </th>
                                <th scope="col">
                                    <div class="tb-center">Name</div>
                                </th>
                                <th scope="col">
                                    <div class="tb-center">Date</div>
                                </th>
                                <th scope="col">
                                    <div class="tb-center">Hits</div>
                                </th>
                            </tr>
                        </thead>
                        <!-- <tbody>
                            <tr>
                                <td>
                                    <div class="tb-center">
                                        2
                                    </div>
                                </td>
                                <td>
                                    <div class="tb-center">
                                        normal
                                    </div>
                                </td>

                                <td>
                                    <div class="tb-left">
                                        <a href="/Board/view/board_name/notice/idx/497/category/4/page/1/n_apply/1"
                                            class="b_subject ">JSM Beauty Global Mall Shipping Notice</a>

                                    </div>
                                </td>
                                <td>
                                    <div class="tb-center">
                                        JSM
                                    </div>
                                </td>
                                <td>
                                    <div class="tb-center"><span title="09:13:50.927994">2018-09-20</span></div>
                                </td>
                                <td>
                                    <div class="tb-center">651</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tb-center">
                                        1
                                    </div>
                                </td>
                                <td>
                                    <div class="tb-center">
                                        normal
                                    </div>
                                </td>

                                <td>
                                    <div class="tb-left">
                                        <a href="/Board/view/board_name/notice/idx/247/category/4/page/1/n_apply/1"
                                            class="b_subject ">Shipping notice for Lunar New Year</a>
                                        (2)

                                    </div>
                                </td>
                                <td>
                                    <div class="tb-center">
                                        JSM Beauty
                                    </div>
                                </td>
                                <td>
                                    <div class="tb-center"><span title="16:22:38.067153">2018-02-07</span></div>
                                </td>
                                <td>
                                    <div class="tb-center">647</div>
                                </td>
                            </tr>
                        </tbody> -->
                    </table>
                </div>
            </div>

        </main>
    </div>
</div>



@endsection