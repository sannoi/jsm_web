@extends('layouts.template.frontend')

<style type="text/css">
#ju-container .ju-page-title {
    margin-top: 179px;
}
</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM Store </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>

@section('content')
<div id="ju-container">
    <div id="ju-content" class="container">
        <div class="ju-page-title">
            <h1 class="entry-title text-gotham text-left">PLOPS</h1>

        </div>
        <div class="bbs-table-view">
            <div class="bbs-box">
                <table summary="">
                    <caption class="blind"></caption>
                    <tbody>
                        <tr>
                            <td>
                                <div class="data-bd-cont">
                                    <div class="clear" id="MS_WritenBySEB">
                                        <div><iframe width="1140" height="550"
                                                src="https://player.vimeo.com/video/288673072?amp;title=0&amp;byline=0&amp;portrait=0"
                                                frameborder="0" allowfullscreen="" 0?=""></iframe></div>
                                        <hr class="clear">

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="media">
                                                    <div class="media-left"><span class="fa-stack fa-lg fa-3x"><i
                                                                class="fa fa-circle fa-stack-2x"></i><i
                                                                class="fa fa-map-marker fa-stack-1x fa-inverse"></i></span>
                                                    </div>
                                                    <div class="media-body">
                                                        <hr class="clear sm">

                                                        <h4 class="media-heading">ADDRESS / ที่อยู่</h4>
                                                        <!-- <p class="letters"></p> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="media">
                                                    <div class="media-left"><span class="fa-stack fa-lg fa-3x"><i
                                                                class="fa fa-circle fa-stack-2x"></i><i
                                                                class="fa fa-phone fa-stack-1x fa-inverse"></i></span>
                                                    </div>
                                                    <div class="media-body">
                                                        <hr class="clear sm">

                                                        <h4 class="media-heading">TEL / เบอร์โทรศัพท์</h4>
                                                        <p class="letters">02-6713-5345</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="clear">
                                        <iframe width="800" height="600"
                                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5421.537410349909!2d127.01998701314504!3d37.52241603540038!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357ca3eb156e8985%3A0x91a6684fd95e1382!2z7ISc7Jq47Yq567OE7IucIOqwleuCqOq1rCDslZXqtazsoJXroZwxMuq4uCA0MA!5e0!3m2!1sko!2skr!4v1495671944783"
                                            frameborder="0" allowfullscreen=""
                                            style="border: 0px currentColor; border-image: none; width: 100%;"></iframe>
                                    </div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="displaynone">
                    <hr size="1" color="#E5E5E5" />
                </div>
            </div>
            <div class="view-link col-sm-4 col-sm-offset-4 ">
                <dl class="bbs-link">
                    <dt></dt>
                    <dd style="margin: 10%;">
                        <a class="displaynone"
                            href="/board/board.html?code=jsmbeauty_image15&page=&board_cate=&type=i">글쓰기</a>
                        <a href="/store"
                            class="btn btn-primary btn-lg btn-block">
                            รายชื่อร้านค้า</a>
                    </dd>
                </dl>
            </div>
            <ul class="list-link displaynone">
                <li>
                    <span class="arrow prev">이전글 :</span>
                    <a
                        href="/board/board.html?code=jsmbeauty_image15&board_cate=&num1=999998&num2=00000&type=v&&s_id=&stext=&ssubject=&shname=&scontent=&sbrand=&sgid=&datekey=&branduid=&lock=N">정샘물
                        플롭스 적립금 혜택!</a>
                </li>
                <li>
                    <span class="arrow next">다음글 :</span>
                </li>
            </ul>
        </div>
    </div>

</div>
</div>

@endsection