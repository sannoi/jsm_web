@extends('layouts.template.frontend')

<style type="text/css">
#ju-container .ju-page-title {
    margin-top: 179px;
}
</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM Store </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>

@section('content')
<div id="ju-container">
    <div id="ju-content" class="container">
        <div class="ju-page-title">
            <h1 class="entry-title text-gotham text-center">Store</h1>

        </div>
        <div class="wpb_single_image wpb_content_element vc_align_center" style="margin-bottom: 10%;"><a href="/Detail/Store"><IMG
                    src="{!!asset('jsmbeauty/src/store/store_001.jpg')!!}"></a>
        </div>
    </div>

</div>
</div>

@endsection