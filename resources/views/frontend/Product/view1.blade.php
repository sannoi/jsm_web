@extends('layouts.template.frontend')

<style type="text/css">
#ju-container .ju-page-title {
    margin-top: 179px;
}

hr {
    margin-top: 23px;
    margin-bottom: 23px;
    border: 0;
    border-top: 1px solid #f0f0f0;
}
</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM Product </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>

@section('content')

@if($ids== '1')


<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">Product</h2>
        <h1 class="product-title text-gotham">JUNGSAEMMOOL Pre-tect Sun Waterfull</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="/jsmbeauty/src/Product/003005000031.jpg"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">JUNGSAEMMOOL Pre-tect Sun Waterfull</figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">JUNGSAEMMOOL Pre-tect Sun Waterfull</h3>
                                SPF50+ / PA++++
                                <br>ครีมกันแดดที่แข็งแกร่งเป็นค่าเริ่มต้น! ผลิตภัณฑ์ดูแลผิวและแต่งหน้า Double Boosting
                                มอยซ์เจอไรเซอร์ให้ความชุ่มชื้นอาทิตย์
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->

            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/sun_top_waterful.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/pre-tect_sun_water.jpg"></div>
                            <div style="margin-bottom: 200px;">
                                <table align="center" style="width: 100%; display: inline;" border="0" cellspacing="0"
                                    cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td colspan="5"><img src="/jsmbeauty/src/DetailProduct/sun_story_top.jpg">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><img src="/jsmbeauty/src/DetailProduct/sun_story_01.jpg">
                                            </td>
                                            <!-- <td>
                                                <a
                                                    href="#"><img
                                                        src="#"></a>
                                            </td> -->
                                            <td><img src="/jsmbeauty/src/DetailProduct/sun_story_01.jpg">
                                            </td>
                                            <!-- <td>
                                                <a
                                                    href="#"><img
                                                        src="#"></a>
                                            </td> -->
                                            <td><img src="/jsmbeauty/src/DetailProduct/sun_story_01.jpg">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '2')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">BASE</h2>
        <h1 class="product-title text-gotham">JUNGSAEMMOOL Artist Contour Palette</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_2.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">JUNGSAEMMOOL Artist Contour Palette</figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">JUNGSAEMMOOL Artist Contour Palette</h3>
                                ช่วยซึมซับเหงื่อ ควบคุมความมัน ให้ผิวดูเรียบเนียน
                                <br>สีที่โปร่งแสงและสว่างขั้นสูงสุด
                                ด้วยเนื้อมุกชิมเมอร์ที่บางเบา โปร่งแสงช่วยให้ผิวดูส่องสว่างโดยไม่หนาเตอะ
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center" style="margin-bottom: 10%;">
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_contour_palette_ko_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_contour_palette_ko_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_history.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_contour_palette_ko_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_use.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_contour_palette_ko_04.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_contour_palette_ko_05.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '3')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">LIP</h2>
        <h1 class="product-title text-gotham">JUNGSAEMMOOL High Tinted Lip Lacquer( Rose Cosset)</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_3.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">JUNGSAEMMOOL High Tinted Lip Lacquer (Rose Cosset)
                        </figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">JUNGSAEMMOOL High Tinted Lip Lacquer( Rose Cosset)</h3>
                                ลิปแลคเกอร์ที่ให้เม็ดสีสดชัด เพิ่มความชุ่มชื้นให้ริมฝีปาก
                                <br>สีสดชัด และติดทนนานลิปแลคเกอร์สามารถซึมเข้าสู่ริมฝีปากเป็นเนื้อเดียวกันทันที
                                พร้อมให้ความชุ่มชื้น ฉ่ำวาว และสีคมชัด
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text">
                                                &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/lip_lacquer_new_01.jpg">
                            </div>
                            <div><img src="/jsmbeauty/src/DetailProduct/lip_lacquer_new_02.jpg">
                            </div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_ingredient.jpg">
                            </div>
                            <div><img src="/jsmbeauty/src/DetailProduct/lip_lacquer_new_05.jpg">
                            </div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_colors.jpg">
                            </div>
                            <div><img src="/jsmbeauty/src/DetailProduct/lip_lacquer_new_07.jpg">
                            </div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_tip_ko.jpg">
                            </div>
                            <div><img src="/jsmbeauty/src/DetailProduct/lip_lacquer_new_11.jpg">
                            </div>
                            <div><img src="/jsmbeauty/src/DetailProduct/lip_lacquer_new_12.jpg">
                            </div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_texture.jpg">
                            </div>
                            <div><img src="/jsmbeauty/src/DetailProduct/lip_lacquer_new_14.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '4')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">LIP</h2>
        <h1 class="product-title text-gotham">JUNGSAEMMOOL Lip-pression #JUST RED</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_4.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">JUNGSAEMMOOL Lip-pression #JUST RED</figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">JUNGSAEMMOOL Lip-pression #JUST RED</h3>

                                <br>
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/lip_pression_1000_ko_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/lip_pression_1000_ko_04.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/lip_pression_1000_ko_05.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/lip_pression_1000_ko_07.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/lip_pression_1000_ko_08.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '5')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">BASE</h2>
        <h1 class="product-title text-gotham">JUNGSAEMMOOL Setting Glowing Base</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_5.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">JUNGSAEMMOOL Setting Glowing Base</figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">JUNGSAEMMOOL Setting Glowing Base</h3>
                                ด้วยส่วนผสมจาก Mineral Complex ช่วยเติมความชุ่มชื้นให้ผิวอย่างสม่ำเสมอ
                                <br>เบสที่ช่วยให้ผิวกระจ่างใสสุขภาพดี ฉ่ำวาวมีชีวิตชีวา เหมาะสำหรับผิวแพ้ง่าย
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_sun_base_04_00.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_glowing_02_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_ingredient.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_history.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_glowing_02_04.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_use.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_glowing_02_06.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_texture_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_glowing_02_08.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '6')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">BASE</h2>
        <h1 class="product-title text-gotham">JUNGSAEMMOOL Setting Smoothing Base</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_6.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">JUNGSAEMMOOL Setting Smoothing Base</figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">JUNGSAEMMOOL Setting Smoothing Base</h3>
                                เมคอัพเบสที่ช่วยให้เครื่องสำอางติดทนนาน พร้อมควบคุมความมัน
                                <br>เบสที่ช่วยให้ผิวกระจ่างใสสุขภาพดี ฉ่ำวาวมีชีวิตชีวา เหมาะสำหรับผิวแพ้ง่าย
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_sun_base_04_00_m.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_smoothing_01_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_smoothing_01_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_ingredient.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_smoothing_01_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_texture.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_smoothing_01_08.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '7')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">BASE</h2>
        <h1 class="product-title text-gotham">JUNGSAEMMOOL Essential Star-cealer Foundation (Fair Light)</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_7.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">JUNGSAEMMOOL Essential Star-cealer Foundation (Fair Light)
                        </figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">JUNGSAEMMOOL Essential Star-cealer Foundation (Fair Light)</h3>
                                ประสิทธิภาพที่ประสานกันระหว่างรองพื้นและคอนซีลเลอร์!
                                <br>รองพื้น: สูตรพิเศษจาก Even Powder Complex & Silicone Elastomer Gel
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->

            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_04.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_05.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_06.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_colors.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_07.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_08.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_use.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_09.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/how_to_use_star_f.gif"><img
                                    src="/jsmbeauty/src/DetailProduct/how_to_use_star_f_02.gif"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_10.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/how_to_use_star_f_03.gif"><img
                                    src="/jsmbeauty/src/DetailProduct/how_to_use_star_f_04.gif"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/atist_tip.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_12.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_tip_ko.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_13.jpg"><img
                                    src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_14.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_texture.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_15.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_foun_pinklight_ko_16.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '8')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">BASE</h2>
        <h1 class="product-title text-gotham">JUNGSAEMMOOL Essential Skin Nuder Cushion (Fair Light)</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_8.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">JUNGSAEMMOOL Essential Skin Nuder Cushion (Fair Light)
                        </figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">JUNGSAEMMOOL Essential Skin Nuder Cushion (Fair Light)</h3>
                                รองพื้นคุชชั่นนี้รับประกันความสามารถในการปกปิดอย่างยาวนาน
                                <br>ด้วยเนื้อสัมผัสที่ชุ่มชื้น
                                ช่วยให้รองพื้นคุชชั่นนี้รับประกันความสามารถในการปกปิดอย่างยาวนาน
                                โดยให้ผิวที่ฉ่ำวาวขั้นสูงสุด
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_04.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_color_g.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_05.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_06.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_07.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_tip_ko.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/use_cushion_01.gif"><img
                                    src="/jsmbeauty/src/DetailProduct/detail_10.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_11.jpg"><img
                                    src="/jsmbeauty/src/DetailProduct/detail_12.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion_01.gif"><img
                                    src="/jsmbeauty/src/DetailProduct/detail_14.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_texture.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_15.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_21.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_22.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/color.gif"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new_04.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new_05.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '9')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">BASE</h2>
        <h1 class="product-title text-gotham">JUNGSAEMMOOL Skin Setting Tone Manner Base SPF 30/ PA+++</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_9.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">JUNGSAEMMOOL Skin Setting Tone Manner Base SPF 30/ PA+++
                        </figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">JUNGSAEMMOOL Skin Setting Tone Manner Base SPF 30/ PA+++</h3>
                                เบสสำหรับผู้ชายออล อิน วัน ที่ครบทั้งให้ความชุ่มชื้น
                                <br>Tea Tree Leaf Extract ช่วยควบคุมความมันส่วนเกิน และช่วยให้ผิวนุ่มนวล
                                สดชื่นเป็นระยะเวลานาน
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_tone_manne_base_ko_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_tone_manne_base_ko_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_tone_manne_base_ko_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_ingredient.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_tone_manne_base_ko_04.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_use.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_tone_manne_base_ko_06.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_toyou_ko.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_tone_manne_base_ko_07.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_texture_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/skin_setting_tone_manne_base_ko_08.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '10')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">BASE</h2>
        <h1 class="product-title text-gotham">Essential Skin Nuder Longwear Cushion (Fair Light)</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_10.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">Essential Skin Nuder Longwear Cushion (Fair Light)</figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">Essential Skin Nuder Longwear Cushion (Fair Light)</h3>
                                รองพื้นคูชชั่นที่ให้เนื้อสัมผัสนุ่มละมุน
                                <br>พัฟแต่งหน้าที่ช่วยเบลนด์สี และสร้างเนื้อสัมผัสใหม่ได้ดั่งใจ
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/long_detail_long_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/long_detail_long_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_color_g.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/long_detail_long_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_07.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_colors_refill.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_06.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_tip_ko.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/use_cushion_01.gif"><img
                                    src="/jsmbeauty/src/DetailProduct/detail_10.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/detail_11.jpg"><img
                                    src="/jsmbeauty/src/DetailProduct/detail_12.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion_01.gif"><img
                                    src="/jsmbeauty/src/DetailProduct/detail_14.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_texture.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/long_detail_long_04.jpg"></div>

                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_21.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_22.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/color.gif"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new_04.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new_05.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '11')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">BASE</h2>
        <h1 class="product-title text-gotham">JUNGSAEMMOOL Cushion-cealer (Fair Light)</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_11.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">JUNGSAEMMOOL Cushion-cealer (Fair Light)</figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">Essential Skin Nuder Longwear Cushion (Fair Light)</h3>
                                คูชชั่นที่เน้นการปกปิด พร้อมด้วย SPF 50+ / PA+++
                                <br>คอนซิลเลอรร์ที่ให้ผิวเปล่งประกาย คอนซิลเลอร์เนื้อบาง
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/use_cushion_01.gif"><img
                                    src="/jsmbeauty/src/DetailProduct/cushion-cushion-cealer_ko_05.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_06.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_color_g.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_08.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_use.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_10.jpg"><img
                                    src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_11.gif"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_13.gif"><img
                                    src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_13.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_14.jpg"><img
                                    src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_15.gif"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_colors_refill.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_17.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_18.jpg"></div>

                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_21.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/cushion-cealer_ko_22.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/color.gif"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new_04.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/eyeshadow_tri_new_05.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '12')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">EYE</h2>
        <h1 class="product-title text-gotham">JUNGSAEMMOOL Cushion-cealer (Fair Light)</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_12.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">JUNGSAEMMOOL Cushion-cealer (Fair Light)</figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">Essential Skin Nuder Longwear Cushion (Fair Light)</h3>
                                ดินสอแบบออล-อิน-วัน
                                <br>ดุจดั่งปลายดินสอของศิลปิน Refining Color Bony Brow
                                ช่วยให้คุณเขียนคิ้วได้สวยเฉียบดั่งใจ
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/color_bony_brow_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/color_bony_brow_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/flash.gif"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/color_bony_brow_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_use.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/color_bony_brow_09.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_texture.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/color_bony_brow_11.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '13')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">TOOL</h2>
        <h1 class="product-title text-gotham">JUNGSAEMMOOL Artist Make-up Blending Tool</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_13.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">JUNGSAEMMOOL Artist Make-up Blending Tool</figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">JUNGSAEMMOOL Artist Make-up Blending Tool</h3>
                                พาเล็ทแต่งหน้าสำหรับช่างแต่งหน้ามืออาชีพ
                                <br>ถาดผสมเครื่องสำอางชิ้นนี้ช่วยให้คุณสามารถผสมและเบลนด์เมคอัพต่างๆ
                                เพื่อให้ได้เนื้อเมคอัพที่ต้องการ
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_make_up_blending_tool.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/012.gif"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '14')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">TOOL</h2>
        <h1 class="product-title text-gotham">JUNGSAEMMOOL Artist Brush Foundation</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_14.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">JUNGSAEMMOOL Artist Brush Foundation</figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">JUNGSAEMMOOL Artist Brush Foundation</h3>
                                เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า!
                                <br>Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_ko_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/Artist_Brush_Foundation_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/Artist_Brush_Foundation_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_ko_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/Artist_Brush_Foundation_05.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_history.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_ko_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_use.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_ko_04.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '15')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">TOOL</h2>
        <h1 class="product-title text-gotham">Artist Brush Contour</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_15.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">Artist Brush Contour</figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">Artist Brush Contour</h3>
                                ไฮไลท์, สโตรค และปัดแก้มได้ด้วย
                                <br>ด้วยแปรงคอนทัวร์ที่ทำจากขนแปรงสังเคราะห์ที่นุ่มนวล ช่วยให้ใบหน้าดูมีมิติ 3D
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_ko_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_Contour_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_Contour_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_ko_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_Contour_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_history.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_ko_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_use.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_Contour_04.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_ko_04.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@elseif($ids== '16')
<div id="ju-container">
    <!-- contents -->

    <div id="ju-content" class="container">
        <div class="ju-page-title">
        </div>
        <h2 class="product-category-title text-gotham">TOOL</h2>
        <h1 class="product-title text-gotham">Artist Brush Hair Line</h1>


        <!-- main -->
        <main class="shopdetail">
            <section class="shopdetailInfo">
                <div class="shopdetailInfoTop">
                    <figure>
                        <div class="thumb clear">
                            <div class="enlarge nomulti"><img src="{!!asset('jsmbeauty/src/product/product_16.jpg')!!}"
                                    style="width: 100%;" alt="JUNGSAEMMOOL Pre-tect Sun Waterfull" /></div>
                        </div>
                        <figcaption class="displaynone">Artist Brush Contour</figcaption>
                    </figure>
                </div>
                <hr>
                <!-- <div class="woocommerce-product-rating">
                    <div class="stary-rating"><span class="b_score_img" style="color: #e51a92;">★★★★★</span></div>
                    <div>(41 รีวิว)</div>
                </div> -->
                <div class="description">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h3 class="product-font">Artist Brush Hair Line</h3>
                                Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น
                                <br> ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="shopdetailInfoBottom">
                    <form name="form1" method="post" id="form1" action="/m/basket.html">
                        <input type="hidden" name="brandcode" value="003005000031" />
                        <input type="hidden" name="branduid" value="2146724" />
                        <input type="hidden" name="xcode" value="008" />
                        <input type="hidden" name="mcode" value="005" />
                        <input type="hidden" name="typep" value="Y" />
                        <input type="hidden" name="ordertype" />
                        <input type="hidden" name="opts" />
                        <input type="hidden" name="mode" />
                        <input type="hidden" name="optioncode" />
                        <input type="hidden" name="optiontype" />
                        <input type="hidden" name="optslist" />
                        <input type="hidden" id="price" name="price" value="0" />
                        <input type="hidden" id="disprice" name="disprice" value="" />
                        <input type="hidden" id="price_wh" name="price_wh" value="0" />
                        <input type="hidden" id="disprice_wh" name="disprice_wh" value="" />
                        <input type="hidden" id="option_type_wh" name="option_type_wh" value="PS" />
                        <input type="hidden" id="prd_hybrid_min" name="prd_hybrid_min" value="1" />
                        <input type="hidden" name="MOBILE_USE" value="YES" />
                        <input type="hidden" name="product_type" id="product_type" value="NORMAL">
                        <input type="hidden" name="multiopt_direct" value="" />
                        <input type="hidden" name="collbasket_type" value="Y" />
                        <input type="hidden" name="package_chk_val" value="0" />
                        <input type="hidden" name="miniq" id="miniq" value="1" />
                        <input type="hidden" name="maxq" id="maxq" value="2147483647" />
                        <input type="hidden" name="cart_free" value="" />
                        <input type="hidden" name="opt_type" value="PS" />
                        <input type="hidden" name="hybrid_op_price" id="hybrid_op_price" value="" />
                        <input type="hidden" name="spcode" /><input type="hidden" name="spcode2" /><input type="hidden"
                            id="regular_price" name="regular_price" value="0" />
                        <input type="hidden" id="discount_price" name="discount_price" value="" />
                        <input type="hidden" id="discount_type" name="discount_type" value="" />
                        <input type="hidden" name="uid" value="2146724" />
                        <input type="hidden" id="option_type" name="option_type" value="PS" />
                        <div class="shopdetailInfoValue">
                            <!-- <p>
                                <span class="shopdetailInfoName">ราคาขาย</span>
                                <span class="shopdetailInfoCont">
                                    <strong class="h4">   0	 &#3647;</strong>
                                </span>
                            </p> -->
                            <!-- <p>
                                <strong class="letters">
                                    이 제품 구매시 <span class="text-danger">2%</span> 포인트 적립!
                                </strong>
                            </p> -->
                            <!-- <p class="displaynone">
                                <span class="shopdetailInfoName">ค่าจัดส่ง</span>
                                <span class="shopdetailInfoCont"><a
                                        href="๒"><span>(조건)</span>
                                        <span class="fa fa-info-circle fa-lg"></span></a>
                                </span>
                            </p> -->
                        </div>
                        <!-- <hr> -->
                        <article class="option_area">

                            <!-- <div class="fixed-btn">
                                <img src="/images/d3/m_04/bt_option@2x.png" width="67" height="18" alt="옵션열기"
                                    class="btn_option" />
                                <a href="#" class="btn_Red">구매하기</a>
                            </div> -->

                            <div class="option_tk">
                                <!-- <div class="shopdetailInfoSelect">
                                    <p>
                                        <span class="shopdetailInfoName">옵션</span>
                                        <span class="shopdetailInfoCont"><select name="optionlist[]"
                                                onchange="change_option(this, 'basic');" label="옵션" opt_type="SELECT"
                                                opt_id="1" opt_mix="N" require="Y" opt_mandatory="Y"
                                                class="basic_option">

                                                <option value="">옵션 선택</option>

                                                <option value="0" title="선 워터풀 50ml SET" matrix="1" price="0" sto_id="0"
                                                    dis_sto_price="" dis_opt_price="" org_opt_price="0">선 워터풀 50ml SET
                                                </option>

                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="mulopt" />
                                    <input type="hidden" name="opttype" />
                                    <input type="hidden" name="optselect" />
                                    <hr>
                                    <p class="shopdetailInfoCount">
                                        <span class="shopdetailInfoName">수량</span>
                                        <span class="shopdetailInfoCont">
                                            <span class="num_c">
                                                <input type="tel" id="MS_amount_basic_0" name="amount[]" value="1"
                                                    class="MS_amount_basic" size="4" style="text-align: right; "
                                                    onchange="set_amount(this, 'basic');" /> <a
                                                    href="javascript:set_amount('MS_amount_basic_0', 'basic', 'up');"
                                                    class="num_up"><i class="fa fa-caret-up"></i></a>
                                                <a href="javascript:set_amount('MS_amount_basic_0', 'basic', 'down');"
                                                    class="num_down"><i class="fa fa-caret-down"></i></a>
                                            </span>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <hr> -->
                                <div class="shopdetailTotal">
                                    <p><strong class="letters">ราคาขาย:</strong> <strong class="h3 text-danger"
                                            style="padding-left:15px;">0<span id="price_text"> &#3647;</span></strong>
                                    </p>
                                </div>
                                <hr class="clear">
                                <div class="shopdetailInfoSelect">

                                </div>

                                <div class="shopdetailInfoMultiSelect">
                                    <!-- 통합옵션 멀티옵션 수량, 가격 -->
                                </div>
                            </div>
                            <!-- <div class="buy_on">
                                <a href="javascript:send('', '');">장바구니</a>
                                <a href="javascript:send('baro', '');" class="buy">구매하기</a>
                            </div> -->
                        </article>

                        <!-- 상품 배송정보 영역 -->
                        <!-- //상품 배송정보 영역 -->
                        <!-- 상품구매 버튼 영역 -->
                        <div class="shopdetailButton">
                            <div class="shopdetailButtonTop">


                                <div class="row">
                                    <div class="col-xs-6"><a href="#" class="btn btn-danger btn-lg btn-block"><i
                                                class="fa fa-credit-card"></i>
                                            <span class="letters">ซื้อเลย</span></a></div>
                                    <div class="col-xs-6"><a href="#" class="btn btn-primary btn-lg btn-block"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span class="letters">ตะกร้าสินค้า</span></a></div>
                                </div>
                            </div>
                            <!-- <div class="shopdetailButtonBottom">
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=FB&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_facebook.gif" alt="페이스북" /></a>
                                <a
                                    href="javascript:window.open('/shop/product_scrap_pop.html?type=TW&uid=2146724', 'name1', 'scrollbars=no,resizeable=no');"><img
                                        src="/images/d3/m_powerpack/32/btn/btn_twitter.gif" alt="트위터" /></a>

                            </div> -->
                            <!-- <div class="naver-checkout">
                                <script type='text/javascript' src='/js/naver_checkout_cts.js'></script>
                                <script type="text/javascript"
                                    src="http://pay.naver.com/customer/js/mobile/naverPayButton.js" charset="UTF-8">
                                </script>
                                <script language="javascript">
                                function nhn_buy_nc_baro() {
                                    var nhnForm = document.form1;
                                    //var nhnForm  = document.getElementById("form1");
                                    if (navigator.appName == 'Microsoft Internet Explorer') {
                                        var ie9_chk = navigator.appVersion.indexOf("MSIE 9") > -1 ? true : false;
                                        var ie10_chk = navigator.appVersion.indexOf("MSIE 10") > -1 ? true : false;

                                        if (ie10_chk == true) {
                                            ie9_chk = true;
                                        }
                                    }
                                    if (ie9_chk || navigator.appName != 'Microsoft Internet Explorer') {
                                        var aElement = document.createElement("input");
                                        aElement.setAttribute('type', 'hidden');
                                        aElement.setAttribute('name', 'navercheckout');
                                        aElement.setAttribute('value', '1');

                                        if (typeof(inflowParam) != 'undefined') {
                                            var aElement2 = document.createElement("input");
                                            aElement2.setAttribute('type', 'hidden');
                                            aElement2.setAttribute('name', 'nhn_ncisy');
                                            aElement2.setAttribute('value', inflowParam);
                                        }
                                    } else {
                                        try {
                                            var aElement = document.createElement(
                                                "<input type='hidden' name='navercheckout' value='1'>");

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement(
                                                    "<input type='hidden' name='nhn_ncisy' value='" + inflowParam +
                                                    "'>");
                                            }
                                        } catch (e) {
                                            var aElement = document.createElement("input");
                                            aElement.setAttribute('type', 'hidden');
                                            aElement.setAttribute('name', 'navercheckout');
                                            aElement.setAttribute('value', '1');

                                            if (typeof(inflowParam) != 'undefined') {
                                                var aElement2 = document.createElement("input");
                                                aElement2.setAttribute('type', 'hidden');
                                                aElement2.setAttribute('name', 'nhn_ncisy');
                                                aElement2.setAttribute('value', inflowParam);
                                            }

                                            ie9_chk = true;
                                        }
                                    }

                                    nhnForm.appendChild(aElement);

                                    if (typeof(inflowParam) != 'undefined') {
                                        nhnForm.appendChild(aElement2);
                                    }
                                    nhnForm.target = "loginiframe";
                                    nhnForm.ordertype.value = "baro|parent.";

                                    send('baro', '');


                                    nhnForm.target = "";
                                    nhnForm.ordertype.value = "";
                                    if (!ie9_chk) {
                                        for (var i = 0; i < nhnForm.navercheckout.length; i++) {
                                            nhnForm.navercheckout[i].value = "";
                                        }
                                    }
                                }

                                function nhn_buy_nc_order() {

                                    var type = "S";
                                    if (type == "N") {
                                        window.open("order.html?navercheckout=2", "");
                                    } else {
                                        location.href = "order.html?navercheckout=2";
                                    }
                                    return false;
                                }

                                function nhn_wishlist_nc(url) {
                                    location.href = url;
                                    return false;
                                }
                                </script>
                                <div id="nhn_btn" style="zoom: 1;">
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    naver.NaverPayButton.apply({
                                        BUTTON_KEY: "2B887B6C-9519-439F-94A2-74981C68AF3C", // 체크아웃에서 제공받은 버튼 인증 키 입력
                                        TYPE: "MA", // 버튼 모음 종류 설정
                                        COLOR: 2, // 버튼 모음의 색 설정
                                        COUNT: 2, // 버튼 개수 설정. 구매하기 버튼만 있으면(장바구니 페이지) 1, 찜하기 버튼도 있으면(상품 상세 페이지) 2를 입력.
                                        ENABLE: "Y", // 품절 등의 이유로 버튼 모음을 비활성화할 때에는 "N" 입력
                                        BUY_BUTTON_HANDLER: nhn_buy_nc_baro, // 구매하기 버튼 이벤트 Handler 함수 등록. 품절인 경우 not_buy_nc 함수 사용
                                        BUY_BUTTON_LINK_URL: "", // 링크 주소 (필요한 경우만 사용)
                                        WISHLIST_BUTTON_HANDLER: nhn_wishlist_nc, // 찜하기 버튼 이벤트 Handler 함수 등록
                                        WISHLIST_BUTTON_LINK_URL: "product.html?mode=wish&branduid=2146724&navercheckout=2", // 찜하기 팝업 링크 주소
                                        EMBED_ID: "nhn_btn",
                                        "": ""
                                    });
                                    //]]>
                                    </script>
                                </div>
                            </div> -->
                        </div>
                        <!-- //상품구매 버튼 영역 -->
                </div>
                </form>
            </section>
            <!-- <div class="text-center" style="margin-top: 40px;">
                <a href="http://www.jsmbeauty.com/m/page.html?id=109"><img
                        src="http://jsmbeauty.img18.kr/event/01_free_event_m.png" class="img-responsive"></a>

            </div> -->
            <hr class="clear">
            <!-- <div class="ncount shopdetailBoard displaynone">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724"><span
                        class="fa fa-comments-o fa-lg"></span> Q&A(24)</a>
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"><span
                        class="fa fa-heart-o fa-lg"></span> 리뷰보드(41)</a>
            </div> -->



            <div class="woocommerce-tabs wc-tabs-wrapper">
                <div id="tab-description"></div>
                <ul class="tabs wc-tabs nav nav-tabs">
                    <li class="description_tab active">
                        <a href="#tab-description" class="text-center">
                            <i class="fa fa-gift fa-2x"></i><br>
                            <span class="letters">คำอธิบาย</span>
                        </a>
                    </li>
                    <!-- <li class="reviews_tab">
                        <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                            class="text-center">
                            <i class="fa fa-commenting-o fa-2x"></i><br>
                            <span class="letters">리뷰 (41)</span>
                        </a>
                    </li> -->
                    <!-- <li class="additional_information_tab">
                        <a href="#tab-additional_shopping_info" class="text-center">
                            <i class="fa fa-info-circle fa-2x"></i><br>
                            <span class="letters">쇼핑정보</span>
                        </a>
                    </li> -->
                    <li class="qna_tab">
                        <a href="#" class="text-center">
                            <i class="fa fa-comments fa-2x"></i><br>
                            <span class="letters">Contact</span>
                        </a>
                    </li>
                </ul>
                <div class="shopdetailItem">
                    <!-- <div class="shopdetailItemPopup">
                        <a href="javascript:MS_pop_shopdetail();"><i class="fa fa-search"></i>
                            รายละเอียดเปิดในหน้าต่างใหม่</a>
                        <p class="shopdetailItemZoom"></p>
                    </div> -->
                    <div class="shopdetailImage">
                        <div id="videotalk_area"></div>
                        <!-- [OPENEDITOR] -->
                        <div class="wpb_single_image wpb_content_element vc_align_center">
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_ko_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/Artist_Brush_Hair_Line_01.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/Artist_Brush_Hair_Line_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_ko_02.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/Artist_Brush_Hair_Line_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_history.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_ko_03.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/product_use.jpg"></div>
                            <div><img src="/jsmbeauty/src/DetailProduct/artist_brush_ko_04.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="clear">


            <!-- <link type="text/css" rel="stylesheet"
                href="/template_common/m_shop/2015_mobile/power_review_custom.1.css?t=201811190941" />
            <div id="mPowerreview">
                <div class="total">
                    <p>총 <span>41개</span>의 상품 리뷰가 있습니다.</p>
                    <div class="score">
                        <span class="star"><em>★★★★★</em></span>
                        <span>5.0</span>
                    </div>
                    <a href="/m/power_review_write.html?product_uid=2146724" class="btn-white">리뷰쓰기</a>
                </div>
                <h3 class="tit">상품리뷰</h3>
                <div class="photo-odr">
                    <a href="javascript:power_review_list_toggle();" class="pr-photo-toggle pr-use-sort "><span>포토리뷰
                            모아보기</span></a>
                    <div class="odr">
                        <select id="PRM_sort">
                            <option value='date'>최신순</option>
                            <option value='score'>평점순</option>
                            <option value='good'>추천순</option>
                        </select> </div>
                </div>
                <div id="listPowerReview" class="MS_power_review_list"></div>
                <div id="MS_review_more_btn_area" style="display: none;"> <a href="javascript:get_more_review_list();"
                        class="btn-more">더보기</a>
                </div>
            </div> -->
            <!-- #mPowerreview -->


    </div>
    <!-- .woocommerce-tabs-info -->
    <!-- <div class="woocommerce-tabs wc-tabs-wrapper">
        <div id="tab-additional_shopping_info"></div>
        <ul class="tabs wc-tabs nav nav-tabs">
            <li class="description_tab">
                <a href="#tab-description" class="text-center">
                    <i class="fa fa-gift fa-2x"></i><br>
                    <span class="letters">설명</span>
                </a>
            </li>
            <li class="reviews_tab">
                <a href="/m/board.html?code=jsmbeauty_board2&search_type=one_product&branduid=2146724"
                    class="text-center">
                    <i class="fa fa-commenting-o fa-2x"></i><br>
                    <span class="letters">리뷰 (41)</span>
                </a>
            </li>
            <li class="additional_information_tab active">
                <a href="#tab-additional_shopping_info" class="text-center">
                    <i class="fa fa-info-circle fa-2x"></i><br>
                    <span class="letters">쇼핑정보</span>
                </a>
            </li>
            <li class="qna_tab">
                <a href="/m/board.html?code=jsmbeauty&search_type=one_product&branduid=2146724" class="text-center">
                    <i class="fa fa-comments fa-2x"></i><br>
                    <span class="letters">Q&amp;A (24)</span>
                </a>
            </li>
        </ul>
        <div class="shopdetailItem">
            <div class="panel entry-content wc-tab">
                <div>
                    <h5>배송관련 유의사항</h5>
                    <ul>
                        <li>배송 마감 시간 : 배송마감 시간은 평일 오후 2시, 토요일, 일요일 주문 시 월요일 오후 2시까지 입니다.</li>
                        <li>발송 전 주문취소는 상품준비 중일 때만 가능합니다. (배송준비 중 상태일 때는 주문취소가 불가합니다. )</li>
                        <li>배송비 : 결제금액이 30,000원 이상인 경우 무료배송 / 30,000원 미만인 경우 2,500원 입니다.</li>
                        <li>제품 발송지 : 경기도 용인시 처인구 원암로 329 (용마로지스-정샘물뷰티)</li>
                        <li>이용 택배사 : 우체국택배</li>
                        <li>주소가 명확하지 않은 경우 연락 없이 반송될 수 있으니 정확한 주소를 기입해주세요.</li>
                        <li>배송 소요시간 : 발송 마감 시간 이전 주문 건은 1~2일 배송(단, 택배사 및 기타 사정으로 인해 2~5일정도 늦어질 수 있습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>교환 및 반품관련 유의사항</h5>
                    <ul>
                        <li>단순 변심, 구매 착오에 따른 교환/반품 신청은 상품을 공급받은 날로부터 7일 이내 가능합니다.(배송 비 고객 부담)</li>
                        <li>공급받은 상품이 사이트 내 제품정보와 다르거나 계약내용과 다르게 이행된 경우에는 상품을 공급받은 날부터 3개월 이내 교환/반품 신청을 할 수 있습니다.(배송 비 회사
                            부담)</li>
                        <li>교환/반품은 My page 내 교환/반품 메뉴를 통해서 직접 신청이 가능합니다.</li>
                        <li>해당 제품 교환/반품 시 사은품 /증정품 등이 제공된 경우에는 상품과 보내 주셔야 합니다.</li>
                        <li>반품 시 상품 대금 환불은 상품 회수가 된 시점으로부터 영업일 7일 이내에 진행 됩니다.</li>
                        <li>제품이 고객의 실수로 인하여 분실 또는 훼손 되었을 경우에는 교환/ 반품이 되지 않으니 주의하시기 바랍니다.</li>
                        <li>고객님께서 제품 사용이 있었을 경우에는 교환/반품이 되지 않습니다.</li>
                        <li>제품을 공급 받고 시간이 오래 지났거나 상품의 패키지가 훼손되어 상품의 가치가 떨어졌을 경우 교환/반품이 되지 않습니다.</li>
                    </ul>
                    <hr class="clear">
                    <h5>불만처리 및 분쟁해결</h5>
                    <ul>
                        <li>교환/반품/환불 등에 대한 문의사항 및 불만처리 요청은 쇼핑몰의 1:1 문의 게시판을 이용하시거나 </li>
                        <li>쇼핑몰 고객센터(080-816-7671)으로 문의 바랍니다. </li>
                        <li>(고객센터 운영시간 평일 : 10:00~05:00/ 점심시간 : 12:00-01:00 / 토요일, 일요일, 공휴일 휴무 )</li>
                        <li>본 사이트에서 판매 되는 모든 상품의 품질보증 및 피해 보상에 관한 사항은 관련법률 및 공정거래위원회 고시</li>
                        <li>[소비자분쟁해결기준]에 따릅니다.</li><br>
                    </ul>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div> -->

    <!-- <section class="shopdetailNotify">
        <h3 class="btn_toggle">상품 고시 정보 <span class="fa fa-caret-down"></span></h3>
        <div class="shopdetailNotifyInfo" style="display:none;">
            <dl>
                <dt>ㆍ용량 또는 중량</dt>
                <dd>50ml</dd>
                <dt>ㆍ제품 주요 사양</dt>
                <dd>모든피부용</dd>
                <dt>ㆍ사용기한 또는 개봉 후 사용기간</dt>
                <dd>개봉 후 12개월 이내 사용 권장(19-05-03 제조, 22-05-102까지)</dd>
                <dt>ㆍ사용방법</dt>
                <dd>스킨 케어의 마지막 단계에서 적량을 취해 피부결을 따라 얼굴 전체에 고루 펴 바릅니다. 펴 바를 때 내용물이 눈에 들어가지 않도록 주의해주세요. (Artist Tip : 기초가
                    완전히 흡수된 뒤에 발라주세요. 제형을 어느정도 롤링 한 뒤에는 손으로 두드리며 펴 발라주면 더욱 쫀쫀하게 밀착되어 그 다음 메이크업이 밀리지 않습니다.)</dd>
                <dt>ㆍ제조자 및 책임판매업자</dt>
                <dd>코스맥스㈜ / ㈜정샘물뷰티</dd>
                <dt>ㆍ제조국</dt>
                <dd>한국</dd>
                <dt>ㆍ모든 원료성분</dt>
                <dd>정제수, 변성알코올, 에칠헥실메톡시신나메이트, 호모살레이트, 부틸렌글라이콜, 에칠헥실살리실레이트, 디에칠아미노하이드록시벤조일헥실벤조에이트, 페네칠벤조에이트, 프로판디올,
                    비스-에칠헥실옥시페놀메톡시페닐트리아진, 폴리실리콘-15, 펜틸렌글라이콜, 메칠메타크릴레이트크로스폴리머, 폴리메칠실세스퀴옥산, 소듐아크릴레이트/소듐아크릴로일디메칠타우레이트코폴리머,
                    글리세린, 이소헥사데칸, 카프릴릴글라이콜, 사이클로펜타실록산,
                    베헤네스-25, 오렌지오일, 디메치콘, 폴리소르베이트80, 아크릴레이트코폴리머, 하이드로제네이티드레시틴, 라벤더오일, 디메치콘/비닐디메치콘크로스폴리머, 슈크로오스디스테아레이트,
                    소르비탄올리에이트, 디소듐이디티에이, 대나무수, 소듐하이알루로네이트크로스폴리머, 디프로필렌글라이콜, 하이드롤라이즈드글라이코사미노글리칸, 소듐하이알루로네이트, 에델바이스꽃/잎추출물,
                    벤질글라이콜, 하이드롤라이즈드하이알루로닉애씨드,
                    에칠헥실글리세린, 목화씨추출물, 하이알루로닉애씨드, 라스베리케톤</dd>
                <dt>ㆍ식품의약품안전처심사 필 유무</dt>
                <dd>SPF50+ / PA++++ [자외선 차단 기능성 화장품]</dd>
                <dt>ㆍ사용할 때 주의사항</dt>
                <dd>1) 화장품 사용 시 또는 사용 후 직사광선에 의하여 사용부위가 붉은 반점, 부어오름 또는 가려움증 등의 이상 증상이나 부작용이 있는 경우 전문의 등과 상담할 것 2) 상처가 있는
                    부위 등에는 사용을 자제할 것 3) 보관 및 취급 시의 주의사항 가) 어린이의 손이 닿지 않는 곳에 보관할 것 나) 직사광선을 피해서 보관할 것</dd>
                <dt>ㆍ품질보증기준</dt>
                <dd>본 상품에 이상이 있을 경우 공정거래위원회 고시 「소비자분쟁해결기준」에 따라 보상해드립니다.</dd>
                <dt>ㆍ소비자상담 관련 전화번호</dt>
                <dd>080-816-7671</dd>
            </dl>
        </div>
    </section> -->
</div>
<!-- noon -->
</main>
<!-- //main -->
</div>
@endif
@endsection