@extends('layouts.template.frontend')

<style type="text/css">
    #ju-container .ju-page-title {
        margin-top: 179px;
    }





</style>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM Product </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>

@section('content')
    <div id="ju-container">
        <div id="ju-content" class="container">
            <div class="ju-page-title">
                <h2 class="product-category-title text-gotham">Product</h2>
                <h1 class="product-title text-gotham">JUNGSAEMMOOL High Tinted Lip Lacquer( Rose Cosset)</h1>
            </div>
            <div id="productDetail">
                <div class="page-body">
                    <div class="thumb-info">
                        <style>
                            /* detailOpt */
                            .prd-info { float: right; width: 534px;}
                            .prd-info .hd{overflow:hidden;}
                            .prd-info .hd h2 {margin:11.5px 0;font-size: 22px; /*border-bottom:2px solid #555;*/word-break:break-word;}
                            .prd-info .hd .summary{font-weight:normal;word-break:break-all;}
                            .prd-info .btns { overflow:hidden;margin:25px 0 10px 0;line-height: 0; }
                            .prd-info .btns img { margin-right: 5px; }
                            .prd-info .prd-price { zoom: 1; overflow: hidden;margin:0; padding:10px 0 0 0; border-top: 1px solid #f0f0f0;/* border-bottom: 1px solid #868686;*/}
                            .prd-info .prd-price dt,
                            .prd-info .prd-price dd { float: left; /*padding:4px 0;*/font-size:14px;height: 37px; line-height: 37px;}
                            .prd-info .prd-price dt { width: 135px; color:#000;}
                            .prd-info .prd-price dd { float:none;width: auto;overflow:hidden; /* text-align:right;*/}
                            .prd-info .prd-price dd em { /*color: #ff0000; font-weight: bold; */}
                            .prd-info .prd-price dd strong { /*color: #000;*/color:#000;font-size:20px;}
                            .prd-info .prd-price dd .event-prc { color: rgb(0, 139, 204); font-weight: bold; }
                            .prd-info .prd-price dd .event-mom { color: #ff0000; font-weight: bold; }
                            .prd-info .opt-sel {/*border-bottom:1px solid #868686;*/margin-bottom:10px;/* width: 305px; margin: 20px auto 0; */}
                            .prd-info .sect { padding:10px 0; /*background-color: #fff; border: 1px solid #e1e1e1;*/ }
                            .prd-info .sect dl { zoom: 1; overflow: hidden; /*margin-top: 2px; padding: 14px 0 8px; background-color: #fff; */}
                            .prd-info .sect dt,
                            .prd-info .sect dd {float: left; font-size:15px;height:37px;margin-bottom:4px;}
                            .prd-info .sect dt { width: 135px;/*margin-bottom:4px;*/line-height:37px;}
                            .prd-info .sect dd { float:none;width: auto;overflow:hidden;/*text-align:right;*/}
                            .prd-info .sect dd select{width:auto !important;
                                display: inline-block;
                                width: auto;
                                vertical-align: middle;
                                height: 37px;
                                padding: 6px 12px;
                                font-size: 14px;
                                line-height: 1.65;
                                color: #555;
                                background-color: #fff;
                                background-image: none;
                                border: 1px solid #ccc;
                                border-radius: 0;
                                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
                                box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
                                -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
                                -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
                                transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
                                /*width: 240px !important;height: 24px; border: 1px solid #ddd;*/
                            }
                            .prd-info .sect .pti {display:none;color: #111; font-weight: bold; margin-bottom:8px;}
                            .prd-info .sect .txt-input { width: 145px; height: 37px; /*9line-height: 17px; */padding-left: 4px; /*border: 1px solid #aaa; color: #888; */}
                            .prd-info .opt-sel .num { zoom: 1; overflow: hidden;/*margin-bottom:10px; */padding:10px 0;}
                            .prd-info .opt-sel .num dt,
                            .prd-info .opt-sel .num dd { float: left; }
                            .prd-info .opt-sel .num dt { width: 135px;padding-top:4px;margin-bottom:5px}
                            .prd-info .opt-sel .num dd {  float:none;width: 74px;overflow:hidden;margin-bottom:4px;}
                            .prd-info .opt-res {/* padding: 15px 10px 0 10px; border-bottom: 1px solid #d5d5d5;*/ }
                            .prd-info .opt-res dl { zoom: 1; margin-bottom:0;overflow: hidden; padding:8px 15px;border-width:1px 1px 0 1px;border-style:solid;border-color:#f0f0f0; }
                            .prd-info .opt-res dt,
                            .prd-info .opt-res dd { /*float: left;*/font-size:15px;display:inline-block;vertical-align:middle;}
                            .prd-info .opt-res dt { width: 210px;word-break: break-all;margin-right: 4px;/* overflow: hidden; white-space: nowrap; text-overflow: ellipsis; */}
                            .prd-info dd.amount { position: relative;padding-right:20px;/* width: 80px; height: 37px;*/ }
                            .prd-info dd.amount .ctrl { position: absolute; top: 0; right: 0;width:20px; /*position:relative;display:inline-block;vertical-align:middle;*//*margin-left:5px;width: 7px; height: 12px; *//*background: url("//www.jsmbeauty.us/storage/jsmbeautyEN/www/frontstore/default_5/EN/Frontend/btn/btn_opt_cnt_bg.gif") no-repeat 0 0; */}
                            .prd-info dd.amount .ctrl a { display:block;height:17px;box-sizing:content-box;line-height:17px;font-size:0;/* height: 6px; text-indent: -9999px; */text-align:center;border:1px solid #ccc;border-left:none;}
                            .prd-info dd.amount .ctrl .up { top: 0; left: 0;border-bottom:none }
                            .prd-info dd.amount .ctrl .up:before{display: inline-block;color:#555;font: normal normal normal 14px/1 FontAwesome;content:"\f0d8"}
                            .prd-info dd.amount .ctrl .dw { bottom: 0; left: 0; }
                            .prd-info dd.amount .ctrl .dw:before{display: inline-block;color:#555;font: normal normal normal 14px/1 FontAwesome;content:"\f0d7"}
                            .prd-info dd.amount .txt-input { width: 40px; height: 35px; line-height: 35px; padding-right: 12px; text-align: right; border: 1px solid #ccc;
                                border-radius: 0;
                                -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
                                box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
                                -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
                                -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
                                transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
                            }
                            .prd-info .opt-res dd.price { width: 170px; margin-right: 8px; color: #000; /*font-size: 14px;*/ font-weight: bold; text-align: right; }
                            .prd-info .opt-res dd.delopt{/*padding-top:4px;*/}
                            .prd-info .opt-res dd.delopt .delete { width: 12px; height: 13px; background: url("//www.jsmbeauty.us/storage/jsmbeautyEN/www/frontstore/default_5/EN/Frontend/btn/btn_opt_del_bg.gif") no-repeat 0 0; border: 0; text-indent: -9999px; display:block}
                            .prd-info .prd-total {overflow:hidden;border-top:1px solid #f0f0f0;padding-top: 25px;text-align:right;}
                            .prd-info .prd-total .abs-price {display:inline-block;font-size:25px;color:#e51a92;padding-left:12px;font-weight:bold;}
                            .prd-info .prd-total .total-text {display:inline-block;font-size:14px;}
                            .prd-info .refer-total { padding-right: 10px; float:right; }
                            .prd-info .line2 {margin-top:20px;border-top: 2px solid #d5d5d5;display:none;}
                            input.basic_option{line-height:18px;height:18px;}
                            .no-opt{font-size: 14px;font-weight: bold;padding-left:10px;height:80px;line-height:80px;}
                            .pg-instructions{margin-top:20px;padding-top:15px;border-top:solid 1px black;}
                            .pg-img{padding:5px;}
                            .prd-info .btns .buy {margin-right:6px;text-align: center;width:122px; height:38px; border:1px solid #000; float: left;line-height:38px;color:#fff; background-color: #000; display:inline-block; font-size:12px; font-weight:bold; }
                            .prd-info .btns .basket {margin-right:6px;text-align: center;width:122px; height:38px; border-left:1px solid #e6e6e6; border-top:1px solid #e6e6e6;  border-right:1px solid #b4b4b4; border-bottom:1px solid #b4b4b4;float: left;line-height:38px;color:#000; background-color: #f9f9f9; display:inline-block; font-size:12px; font-weight:bold; }
                            .prd-info .btns .wish  { text-align: center;width:122px; height:38px; border-left:1px solid #e6e6e6; border-top:1px solid #e6e6e6;  border-right:1px solid #b4b4b4; border-bottom:1px solid #b4b4b4;float: left;line-height:38px;color:#000; background-color: #fff; display:inline-block; font-size:12px; font-weight:bold; }
                            .prd-info .another_buy {height: 46px; line-height:46px; font-size:16px; color: #fff; font-weight: bold; padding: 0 110px; display:block; cursor:pointer; background-color:#3f3f3f;text-align: center;}
                        </style>
                        <script type="text/javascript">
                            $(function(){
                                makeshop.init({
                                    xid_info: {"xid":385,"mall_id":"jsmbeautyMASTER","real_qty":null,"manage_code":"","order_min":null,"order_max":null,"sale_term":"N","basic_option_use":"Y","basic_option_comb":"N","basic_option_show":"multi","basic_option_order":"reg","basic_option_stock":"N","add_option_use":"N","add_option_order":"reg","total_in":0,"total_out":0,"reserve_qty":null,"pay_done_qty":0,"common_list_img":null,"common_detail_img":null},
                                    add: [],
                                    select: {"k9998":{"name":"\uc635\uc158","value":"Pro-lastingFinishPowder SET","idx":9998,"add_price":0,"status":"S","xid":3390}},
                                    select_nc: [],
                                    select_value: [],
                                    apkg_necess: [],
                                    currency: ' USD',
                                    select_lng: 1,
                                    select_nc_lng: 0,
                                    insert_lng: 0,
                                    exchange : {"standard_currency":"USD","base_currency":"KRW","system_currency":"KRW","money_unit":{"system":"\uffe6","base":"\uffe6","stand":"\uff04","refer":null,"per_base_currency":"1","pre_stand":null,"pre_refer":null,"post_stand":" USD","post_refer":" "},"money_unit_cnt":{"base":0,"stand":2,"refer":2},"rate_code":{"base":"KRW2KRW","stand":"USD2KRW","refer":"USD2"},"exchange":{"rate":{"stand":"1089.40","refer":0},"margin":{"stand":0}}},
                                    show_auth:"Y"		,pdt_price:39.65	});
                                $("input[name='pdt-cnt']").val(0);
                                resetCount(1);

                            });
                        </script>

                    </div><!-- .thumb-info -->
                    <div>

                    </div>

                    <div style="margin-top: 30px;">

                    </div>
                </div>
                <div class="grid">
                    <div class="row">
                        <div class="col-lg-6 col-md-3 col-sm-3 col-xs-12">
                            <div class="thumb-wrap">
                                <div class="thumb"><a href="javascript:;"><img src="{!!asset('jsmbeauty/src/pic/3_2.jpg')!!}" /></a></div>
                            </div><!-- .thumb-wrap -->
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
                            <div class="prd-info">
                                <div class="hd">
                                    <p class="summary"></p>
                                </div>
                                <dl class="prd-price">

                                    <dt>ราคา</dt>
                                    <dd>

                                        <strong>xx.xx บาท</strong>
                                    </dd>
                                    <dt>ทดสอบ</dt>
                                    <dd>xx.xx บาท
                                    </dd>

                                </dl>
                                <!--
                                <div class="opt-sel">
                                    <div class="sect">
                                        <p class="pti">Option</p>
                                        <dl>
                                            <dt >옵션 : </dt>
                                            <dd >
                                                <select name="select_option[]" class='basic_option' style="width: 150px">
                                                    <option value="" price="">Select an option.</option>
                                                    <option value="9998" price="0" disabled>
                                                        Pro-lastingFinishPowder SET
                                                        - sold out						</option>
                                                </select>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                                -->
                                <div class="line2"></div>
                                <form name="cartform" method="post" action="">
                                    <input type="hidden" name="mode" value="" />
                                    <input type="hidden" name="item_flag" value="" />
                                    <input type="hidden" name="cid[]" value="80" />
                                    <input type="hidden" name="pid[]" value="1170" />
                                    <div class="opt-res" id="add-opt-res" style="display:none"></div>
                                    <div class="opt-res" id="add-opt-res-add" style="display:none"></div>
                                </form>

                                <div class="prd-total">
                                    <p class="total-text">Total Purchase Amount:</p>
                                    <p class="abs-price"><span id="res-total">0</span> บาท</p>
                                </div>



                                <!-- 장바구니 알림 레이어 시작 -->
                                <style>
                                    @import url(/Common/css/font-awesome.css);

                                    .cart_sale_wrap {position:absolute; top:100%; z-index:1000; display:none;}
                                    .cart_sale {position:relative; top:0; left:0;  background-color:#fff;}
                                    .cart_sale ul {overflow:hidden;}
                                    .cart_sale ul .sale_text {background-color:#000; height:30px; line-height:30px; color:#fff; text-align:center;}
                                    .cart_sale ul .sale_text i {width: 15px; height: 15px; vertical-align: middle; margin: 0 10px 0px 0;}
                                    .cart_sale ul .sale_text span {display: inline-block; zoom:1; *display:inline;  font-size: 12px;}
                                    .cart_sale ul .bar {padding:3%; font-size:11px; border: 1px solid #d7dce0;   -webkit-border-radius: 5px; border-radius: 5px; border-top:0; border-top-left-radius: 0; -webkit-border-top-left-radius:0; border-top-right-radius: 0; -webkit-border-top-right-radius:0; font-size:0; display:block; font-size:12px;}
                                    .cart_sale ul .bar .sale_discount {margin:0 0 5px 0; font-size:12px; font-weight:bold; color:#000; font-family:Dotum, AppleGothic, Helvetica, sans-serif;}
                                    .cart_sale ul .bar .gauge_wrap {border: 1px solid #d7dce0; height:15px; overflow:hidden;}
                                    .cart_sale ul .bar .gauge_wrap .gauge {background-color:#FFC000; height:15px; display:block;}
                                    .cart_sale .sale_close {position:absolute; top:9px; right:11px; margin:0; font-weight:bold; cursor:pointer;  font-family:Dotum, AppleGothic, Helvetica, sans-serif; color:#fff; font-size:11px;}
                                    .cart_sale .fas {font: normal normal normal 12px/1 FontAwesome;}
                                </style>

                                <script>
                                    String.prototype.unformat = function(){
                                        var str = this.replace(/,/g,'');
                                        var num = parseFloat(str);
                                        if( isNaN(num) ) {
                                            return "0";
                                        }

                                        return String(num);
                                    };

                                    $(function(){
                                        $(".detail_cart").click(function(){
                                            setTimeout(function(){
                                                if ( $("#quick_view2").length > 0 )
                                                {
                                                    $(".cart_sale_wrap").css({"width":$("#quickWrapper").width(),"left":$("#quickWrapper").offset().left});
                                                    $(".cart_sale_wrap").show().animate({top: $("#quickWrapper").offset().top + ( $("#quickWrapper").height()+30 ) },1000);

                                                    if ( $(".cart_sale_wrap").css("display") == "block" )
                                                    {
                                                        $("html").click(function(e){
                                                            if ( $(".cart_sale_wrap, .detail_cart").has(e.target).length === 0 )
                                                            {
                                                                cartsale_close();
                                                            }
                                                        });
                                                    }
                                                    $("#quick_view2").before( $(".cart_sale_wrap") );
                                                }
                                            },1000);

                                            total_text = $("#res-total").text().unformat();
                                            dis_text = $(".cart_sale ul .bar .sale_discount span").text();
                                            per_text = ( (0 + Number(total_text) )/(150 * 0.01) ); // 무료배송기준

                                            $(".cart_sale ul .bar .sale_discount span").text( Math.round( (dis_text - Number( total_text) ) * 100 ) / 100 );
                                            $(".cart_sale ul .bar .gauge_wrap .gauge").css("width", per_text + "%" );
                                            $(".cart_sale ul .bar .sale_discount span:contains('-')").text("0");
                                            $(".cart_sale ul .bar .sale_discount span:contains('NaN')").text("0");

                                            if ( $(".cart_sale_wrap .cart_sale ul .bar .sale_discount").length == false ){
                                                $(".cart_sale_wrap .cart_sale .bar").html("refresh");
                                            }

                                        });
                                    });

                                    function cartsale_close(){
                                        $(".cart_sale_wrap").css({"display":"none","top":"100%"});
                                        $(".cart_sale_wrap .cart_sale .bar .sale_discount").remove();
                                    }
                                </script>

                                <div class="cart_sale_wrap">
                                    <div class="cart_sale">
                                        <ul>
                                            <li class="sale_text">
                                                <i class="fas fa-shopping-bag"></i>
                                                <span>
												FREE SHIPPING
											</span>
                                            </li>
                                            <li class="bar">
                                                <div class="sale_discount">
                                                    Shopping cart Discount (left to $ <span>150</span>  → )
                                                </div>
                                                <div class="gauge_wrap"><span class="gauge"></span></div>
                                            </li>
                                        </ul>
                                        <div class="sale_close" onclick="cartsale_close();">X</div>
                                    </div>
                                </div>

                                <div class='facebook-btn'  onclick=makeshop.commPopupOpen('/Member/Auth/popup/api/facebook','',750,540);><img src= //www.jsmbeauty.us/storage/jsmbeautyEN/www/frontstore/default_5/EN/Frontend/btn/btn_facebook.png alt='' ><span>Facebook Login</span></div>


                                <div class='btn-google' onclick=makeshop.commPopupOpen('/Member/Auth/popup/api/google','',750,540);><img src= //www.jsmbeauty.us/storage/jsmbeautyEN/www/frontstore/default_5/EN/Frontend/btn/btn_google_login.png alt=''><span>Google Login</span></div>





                                <div class="prd-pg">
                                    <img src='//www.jsmbeauty.us/storage/jsmbeautyEN/www/frontstore/default_5/EN/Frontend/Product/Detail/visa.png' class='pg-img'><img src='//www.jsmbeauty.us/storage/jsmbeautyEN/www/frontstore/default_5/EN/Frontend/Product/Detail/mastercard.png' class='pg-img'><img src='//www.jsmbeauty.us/storage/jsmbeautyEN/www/frontstore/default_5/EN/Frontend/Product/Detail/jcb.png' class='pg-img'><img src='//www.jsmbeauty.us/storage/jsmbeautyEN/www/frontstore/default_5/EN/Frontend/Product/Detail/discover.png' class='pg-img'><img src='//www.jsmbeauty.us/storage/jsmbeautyEN/www/frontstore/default_5/EN/Frontend/Product/Detail/american_express.png' class='pg-img'><img src='//www.jsmbeauty.us/storage/jsmbeautyEN/www/frontstore/default_5/EN/Frontend/Product/Detail/paypal.png' class='pg-img'>
                                </div>
                            </div>                        </div><!-- .thumb-info -->
                    </div>
                </div>
                <div class="grid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <ul class="tabs wc-tabs nav nav-tabs">
                                <li class="description_tab active">
                                    <a href="#tab-description" class="text-center">
                                        <i class="fa fa-gift fa-2x"></i>
                                        <span class="letters">DETAIL</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="panel_box">
                                <div class="panel entry-content wc-tab">
                                    <div class="wc-tab-inner">
                                        <style>
                                            .deli_infor{padding:30px 10px;font-size:13px;clear:both;}
                                            .deli_infor ul{}
                                            .deli_infor ul li p{position:relative;margin:3px 0;padding-left:15px;line-height:1.5em;}
                                            .deli_infor ul li p.tit{padding-left:0;background:none;margin-top:15px;}
                                        </style>
                                        <div class="deli_infor" id="detailReview">
                                            <ul style="padding-bottom:40px;border-bottom:1px dashed #ccc;text-align:left;">
                                                <li>
                                                    <p class="tit" style="margin-top:0">High Tinted Lip Lacquer</p>
                                                    <p>- ลิปแลคเกอร์ที่ให้เม็ดสีสดชัด เพิ่มความชุ่มชื้นให้ริมฝีปาก แต่ไม่เหนียวเหนอะหนะ.</p>
                                                    <p>- สีสดชัด และติดทนนานเพียงแค่แต้มลงบนริมฝีปาก ลิปแลคเกอร์สามารถซึมเข้าสู่ริมฝีปากเป็นเนื้อเดียวกันทันที พร้อมให้ความชุ่มชื้น ฉ่ำวาว และสีคมชัด .</p>
                                                    <p>- ทินท์ที่ช่วยบำรุงริมฝีปากด้วยเนื้อกึ่งน้ำกึ่งน้ำมันด้วยสูตรพิเศษที่ทำจากสารสกัดธรรมชาติ ช่วยสร้างปราการเก็บกักความชุ่มชื้นให้ริมฝีปากได้ยาวนาน .</p>
                                                    <p>- ให้ความรู้สึกเบาสบาย และเรียบลื่นเนื้อสัมผัสที่เคลือบบนริมฝีปากอย่างบางเบา ช่วยให้ริมฝีปากไม่แห้งแตก หรือ หยาบกร้าน .</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!-- .page-body -->
        </div><!-- #productDetail -->
        <!-- //content -->


@endsection


