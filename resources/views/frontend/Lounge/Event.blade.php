@extends('layouts.template.frontend')

<style type="text/css">
#ju-container .ju-page-title {
    margin-top: 179px;
}
</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM PRODUCT </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>

@section('content')

<div id="ju-container">
    <div id="ju-content" class="container">
        <div class="ju-page-title">
            <h1 class="entry-title">กิจกรรม</h1>
            <div>JUNGSAEMMOOL กิจกรรมแห่งความสวย</div>
        </div>

        <div class="title">
            <!doctype html>
            <html>
            <head>
                <meta charset="utf-8">
                <title>อีเว็นท์</title>
            </head>

            <body>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="/event"><strong>กิจกรรมปัจจุบัน</strong></a></li>
                    <li><a href="/event2" class="letters"><strong>กิจกรรมที่ผ่านมา</strong></a></li>
                </ul>
                <hr class="clear">
                <a
                    href="http://www.jsmbeauty.com/shop/shopdetail.html?branduid=2154242&xcode=008&mcode=002&scode=&type=Y&sort=order&cur_code=008002&GfDT=aWV5"><img
                        src="jsmbeauty/src/Event_menu/1130_490_01.jpg"></a>
            </body>
            </html>
        </div>
        <hr class="clear">
        <div class="row">
            <div class="col-md-6">
                <div class="thumbnail event">
                    <a class="thumnail_img"
                        href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=10&num1=999768&num2=00000&number=5&lock=N">
                        <img src="jsmbeauty/src/Event_menu/562x314_02_2.jpg" class="img-responsive"> </a>
                    <div class="caption">
                        <h5 class="letters">
                            <a href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=10&num1=999768&num2=00000&number=5&lock=N"
                                class="text-primary" rel="bookmark"><i class="fa fa-calendar"></i>
                               </a>
                            <span class="label label-danger text-white"></span> </h5>
                        <p>
                            <span class="label label-danger text-white letters"></span>
                        </p>
                    </div>
                </div>
            </div>
            <!--.col-md-6-->
            <div class="col-md-6">
                <div class="thumbnail event">
                    <a class="thumnail_img"
                        href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=10&num1=999769&num2=00000&number=4&lock=N">
                        <img src="jsmbeauty/src/Event_menu/562x314_01.jpg" class="img-responsive"> </a>
                    <div class="caption">
                        <h5 class="letters">
                            <a href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=10&num1=999769&num2=00000&number=4&lock=N"
                                class="text-primary" rel="bookmark"><i class="fa fa-calendar"></i>
                                </a>
                        </h5>
                        <p>
                            <span class="label label-danger text-white letters"></span>
                        </p>
                    </div>
                </div>
            </div>
            <!--.col-md-6-->
            <div class="col-md-6">
                <div class="thumbnail event">
                    <a class="thumnail_img"
                        href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=10&num1=999770&num2=00000&number=3&lock=N">
                        <img src="jsmbeauty/src/Event_menu/event_06.jpg" class="img-responsive"> </a>
                    <div class="caption">
                        <h5 class="letters">
                            <a href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=10&num1=999770&num2=00000&number=3&lock=N"
                                class="text-primary" rel="bookmark"><i class="fa fa-calendar"></i>
                                </a>
                        </h5>
                        <p>
                            <span class="label label-danger text-white letters"></span>
                        </p>
                    </div>
                </div>
            </div>
            <!--.col-md-6-->
            <div class="col-md-6">
                <div class="thumbnail event">
                    <a class="thumnail_img"
                        href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=10&num1=999771&num2=00000&number=2&lock=N">
                        <img src="jsmbeauty/src/Event_menu/gift_562x314.jpg" class="img-responsive"> </a>
                    <div class="caption">
                        <h5 class="letters">
                            <a href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=10&num1=999771&num2=00000&number=2&lock=N"
                                class="text-primary" rel="bookmark"><i class="fa fa-calendar"></i>
                               </a>
                        </h5>
                        <p>
                            <span class="label label-danger text-white letters"></span>
                        </p>
                    </div>
                </div>
            </div>
            <!--.col-md-6-->
            <div class="col-md-6">
                <div class="thumbnail event">
                    <a class="thumnail_img"
                        href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=10&num1=999795&num2=00000&number=1&lock=N">
                        <img src="jsmbeauty/src/Event_menu/Group-42_3.jpg" class="img-responsive"> </a>
                    <div class="caption">
                        <h5 class="letters">
                            <a href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=10&num1=999795&num2=00000&number=1&lock=N"
                                class="text-primary" rel="bookmark"><i class="fa fa-calendar"></i> </a>
                            <span class="label label-danger text-white"></span> </h5>
                        <p>
                            <span class="label label-danger text-white letters"></span>
                        </p>
                    </div>
                </div>
            </div>
            <!--.col-md-6-->
        </div>
        <nav class="text-center">
            <ul class="pagination">
                <li><span class="page-numbers current">1</span></li>
            </ul>
        </nav>
        <dl class="bbs-link bbs-link-btm displaynone">
            <dd>
                <a class="write" href="/board/board.html?code=jsmbeauty_image1&page=1&board_cate=10&type=i">ระบุ</a>
            </dd>
        </dl>
    </div>
    <!--#ju-content-->
</div>
<!--#ju-container-->


@endsection