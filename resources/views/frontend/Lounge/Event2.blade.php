@extends('layouts.template.frontend')

<style type="text/css">
#ju-container .ju-page-title {
    margin-top: 179px;
}
</style>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSM ARTIST </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
</head>

@section('content')
<div id="ju-container">
    <div id="ju-content" class="container">
        <div class="ju-page-title">
            <h1 class="entry-title">กิจกรรม</h1>
            <div>JUNGSAEMMOOL กิจกรรมแห่งความสวย</div>
        </div>

        <div class="title">
            <ul class="nav nav-tabs">
                <li><a href="/event"><strong>กิจกรรมปัจจุบัน</strong></a></li>
                <li class="active"><a href="/event2" class="letters"><strong>กิจกรรมที่ผ่านมา</strong></a></li>
            </ul>
            <hr class="clear">
        </div>

        <hr class="clear">
        <div class="row">
            <div class="col-md-6">
                <div class="thumbnail event">
                    <a class="thumnail_img"
                        href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=11&num1=999772&num2=00000&number=214&lock=N">
                        <img src="jsmbeauty/src/Event_menu/Group_7_2.jpg" class="img-responsive"> </a>
                    <div class="caption">
                        <h5 class="letters">
                            <a href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=11&num1=999772&num2=00000&number=214&lock=N"
                                class="text-primary" rel="bookmark"><i class="fa fa-calendar"></i></a>
                            <span class="label label-danger text-white"></span> </h5>
                        <p>
                            <span class="label label-danger text-white letters"></span>
                        </p>
                    </div>
                </div>
            </div>
            <!--.col-md-6-->
            <div class="col-md-6">
                <div class="thumbnail event">
                    <a class="thumnail_img"
                        href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=11&num1=999773&num2=00000&number=213&lock=N">
                        <img src="/jsmbeauty/src/Event_menu/Group-3_1.jpg" class="img-responsive"> </a>
                    <div class="caption">
                        <h5 class="letters">
                            <a href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=11&num1=999773&num2=00000&number=213&lock=N"
                                class="text-primary" rel="bookmark"><i class="fa fa-calendar"></i></a>
                        </h5>
                        <p>
                            <span class="label label-danger text-white letters"></span>
                        </p>
                    </div>
                </div>

            </div>
            <!--.col-md-6-->
            <div class="col-md-6">
                <div class="thumbnail event">
                    <a class="thumnail_img"
                        href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=11&num1=999775&num2=00000&number=211&lock=N">
                        <img src="jsmbeauty/src/Event_menu/Group-5.jpg" class="img-responsive"> </a>
                    <div class="caption">
                        <h5 class="letters">
                            <a href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=11&num1=999775&num2=00000&number=211&lock=N"
                                class="text-primary" rel="bookmark"><i class="fa fa-calendar"></i></a>
                        </h5>
                        <p>
                            <span class="label label-danger text-white letters"></span>
                        </p>
                    </div>
                </div>
            </div>
            <!--.col-md-6-->
            <div class="col-md-6">
                <div class="thumbnail event">
                    <a class="thumnail_img"
                        href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=11&num1=999776&num2=00000&number=210&lock=N">
                        <img src="jsmbeauty/src/Event_menu/Group-4_1.jpg" class="img-responsive"> </a>
                    <div class="caption">
                        <h5 class="letters">
                            <a href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=11&num1=999776&num2=00000&number=210&lock=N"
                                class="text-primary" rel="bookmark"><i class="fa fa-calendar"></i></a>
                            <span class="label label-danger text-white"></span> </h5>
                        <p>
                            <span class="label label-danger text-white letters"></span>
                        </p>
                    </div>
                </div>
            </div>
            <!--.col-md-6-->
            <div class="col-md-6">
                <div class="thumbnail event">
                    <a class="thumnail_img"
                        href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=11&num1=999777&num2=00000&number=209&lock=N">
                        <img src="jsmbeauty/src/Event_menu/Group-2_2.jpg" class="img-responsive"> </a>
                    <div class="caption">
                        <h5 class="letters">
                            <a href="/board/board.html?code=jsmbeauty_image1&page=1&type=v&board_cate=11&num1=999777&num2=00000&number=209&lock=N"
                                class="text-primary" rel="bookmark"><i class="fa fa-calendar"></i> </a>
                            <span class="label label-danger text-white"></span> </h5>
                        <p>
                            <span class="label label-danger text-white letters"></span>
                        </p>
                    </div>
                </div>
            </div>
            <!--.col-md-6-->
        </div>
        <nav class="text-center">
            <ul class="pagination">
                <li><span class="page-numbers current">1</span></li>
                <li><a href="/event2page2" class="page-numbers">2</a></li>
            </ul>
        </nav>
        <dl class="bbs-link bbs-link-btm displaynone">
            <dd>
                <a class="write" href="/board/board.html?code=jsmbeauty_image1&page=1&board_cate=11&type=i">ระบุ</a>
            </dd>
        </dl>
    </div>
    <!--#ju-content-->
</div>


@endsection