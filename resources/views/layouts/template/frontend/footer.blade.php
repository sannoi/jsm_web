<footer id="ju-footer">
    <div class="ju-footer-wrap">
        <div class="container">
            <div class="row text-gotham">
                <div class="col-lg-2 col-md-4 col-xs-6">
                    <div class="footer-widget">
                        <h5>JUNGSAEMMOOL</h5>
                        <div class="menu-footer01-container">
                            <ul id="menu-footer01" class="menu">
                                <li class="menu-item"><a href="">Brand JSM</a></li>
                                <li class="menu-item"><a href="">Artist JSM</a></li>
                                <li class="menu-item"><a href="">STORE</a></li>
                                <li class="menu-item"><a href="">ART&ACADEMY</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- .footer-widget-->
                </div>
                <!-- .col-lg-2 col-md-4 col-xs-6-->

                <div class="col-lg-2 col-md-4 col-xs-6">
                    <div class="footer-widget">
                        <h5><a href="">PRODUCT</a></h5>
                        <div class="menu-footer03-container">
                            <ul id="menu-footer03" class="menu">
                                <li id="menu-item-562" class="menu-item">
                                    <a href="">BASE MAKE UP</a>
                                </li>
                                <li id="menu-item-564" class="menu-item">
                                    <a href="">LIP MAKE UP</a>
                                </li>
                                <li id="menu-item-563" class="menu-item">
                                    <a href="">EYE MAKE UP</a>
                                </li>
                                <li id="menu-item-565" class="menu-item">
                                    <a href="">SKINCARE</a>
                                </li>
                                <li id="menu-item-10071" class="menu-item">
                                    <a href="">Tool</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-xs-6">
                    <div class="footer-widget">
                        <h5>LOUNGE</h5>
                        <div class="menu-footer04-container">
                            <ul id="menu-footer04" class="menu">
                                <!-- <li class="menu-item"><a href="">TEST</a></li> -->
                                <li class="menu-item"><a href="">EVENT STORE</a></li>
                                <li class="menu-item"><a href="">LINE STORY</a></li>
                                <!-- <li class="menu-item"><a href="">MEMBERSHIP</a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-xs-6">
                    <div class="footer-widget">
                        <h5>CONTACT</h5>
                        <div class="menu-footer05-container">
                            <ul id="menu-footer05" class="menu">
                                <li class="menu-item"><a href="">NOTICE</a></li>
                                <li class="menu-item"><a href="">FAQ</a></li>
                                <li class="menu-item"><a href="">1:1 Q&#038;A</a></li>
                                <li class="menu-item"><a href="">REVIEW</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-xs-6">
                    <div class="footer-widget">
                        <h5>MY PAGE</h5>
                        <div class="menu-footer06-container">
                            <ul id="menu-footer06" class="menu">
                                <li class="menu-item">
                                    <ul class="sub-menu" style="height: 185px;">
                                        <li class="menu-item">
                                            <a href="">login</a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="">MY PAGE</a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="">ORDER list</a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="">CART</a>
                                        </li>
                                    </ul>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">

            <div class="container">
                <p class="text-center">
                    <a href="#"><img src="{!!asset('jsmbeauty/src/logo_jungsammool_white.png')!!}"
                            style="max-width: 85px; height: auto;"></a>
                </p>
                <p class="footer-social-link text-center">
                    <a target="_blank" href="https://www.facebook.com/jungsaemmoolThailand/" class="btn btn-facebook"
                        data-toggle="tooltip" data-placement="top" title="" data-original-title="페이스북"><img
                            src="/jsmbeauty/src/Icon/5.png" style=""></a>
                    <a target="_blank" href="https://www.instagram.com/jsmbeauty_th" class="btn btn-instagram"
                        data-toggle="tooltip" data-placement="top" title="" data-original-title="인스타그램"><img
                            src="/jsmbeauty/src/Icon/6.png" style=""></a>
                </p>
                <div class="text-center">
                    <ul id="menu-footer-menu" class="list-inline">
                        <li id="menu-item-342" class="menu-item">
                            <a href="/m/contract.html">이용약관</a>
                        </li>
                        <li id="menu-item-341" class="menu-item">
                            <a href="/m/privacy.html">개인정보 처리방침</a>
                        </li>
                    </ul>
                </div>
                <p class="text-center text-muted">
                    <small>
                        사업자등록번호 : 261-81-20935 <br>
                        통신판매업신고 : 2015-서울강남02896 <a href="javascript:;" onclick="onopen(2618120935);"
                            target="_blank">사업자정보확인</a><br>
                        개인정보보호최고책임자 : 유민석<br>
                        서울시 강남구 압구정로72길 8 백원빌딩 3층 <br>
                        상호명: 주식회사 정샘물뷰티, 대표자:유민석 <br>
                        jungsaemmool beauty 3F 8, Apgujeong-ro 72-gil, <br>
                        Gangnam-gu, SEOUL, KOREA <br>
                        TEL : 080-816-7671 <br>
                        E-Mail : 마케팅 제안, 업무제휴 : jsmbeauty@jsmbeauty.com <br>
                        공식CS(상품, 주문관련 그 외) : jsmbeauty_CS@jsmbeauty.com
                    </small>
                </p>
                <p class="text-center text-muted">
                    <small>COPYRIGHTⓒ2015 JUNGSAEMMOOL.COM ALL RIGHT RESERVERD.</small>
                </p>

                <div class="text-center">
                    <img src="{!!asset('jsmbeauty/src/Icon/visa.png')!!}">
                    <img src="{!!asset('jsmbeauty/src/Icon/mastercard.png')!!}">
                </div>

            </div>

        </div>

    </div>
    </div>
</footer>