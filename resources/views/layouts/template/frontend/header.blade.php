<header id="ju-header">
    <!-- top bar -->
    <div id="topBar">
        <div class="container">
            <nav class="text-right clearfix text-gotham">
                <ul id="menu-topbar" class="list-inline">
                    <li><a href=""><img src="/jsmbeauty/src/Icon/1.png" style="width: 20px;"> LOGIN</a></li>
                    <li><a href=""><img src="/jsmbeauty/src/Icon/2.png" style="width: 20px;"> JOIN</a></li>
                    <li><a href=""><img src="/jsmbeauty/src/Icon/3.png" style="width: 20px;"> ORDER</a></li>
                </ul>
                <div class="btn-group pull-right" role="group" aria-label="...">
                    <a href="" style="display:block !important;" class="btn btn-primary btn-sm cart" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="TEST">
                    <img src="/jsmbeauty/src/Icon/4.png" style="width: 20px;"> <span class="badge"><span id="user_basket_quantity" class="user_basket_quantity">0</span></span>
                    </a>
                    <a href="" class="btn btn-primary btn-sm" style="border-left: 1px solid rgba(255,255,255,0.2);padding-top: 4px; padding-bottom: 8px;">
                        <strong>TH</strong>
                    </a>
                    <a href="" class="btn btn-primary btn-sm" style="border-left: 1px solid rgba(255,255,255,0.2);padding-top: 4px; padding-bottom: 8px;">
                        <strong>English</strong>
                    </a>
                    <!-- <a href="" class="btn btn-primary btn-sm" style="border-left: 1px solid rgba(255,255,255,0.2);padding-top: 4px; padding-bottom: 8px;">
                        <strong>TEST</strong>
                    </a> -->
                </div>
            </nav>
        </div>
    </div>
    <!-- //top bar -->

    <div id="ju-header-navi" data-spy="affix" data-offset-top="150" class="affix-top">
        <div class="ju-header-navi-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-2">
                        <div class="logo">
                            <a href="/"><img src="{!!asset('jsmbeauty/src/logo1.png')!!}" class="img-responsive"></a>
                            <div class="visible-xs-block visible-sm-block mobile-menu-btn">
                                <button type="button" class="btn-header-search-mobile btn btn-default" data-toggle="modal" data-target="#juSearch">
                                    <i class="fa fa-search"></i>
                                </button>
                                <button type="button" class="btn-header-menu-mobile btn btn-default">
                                    <i class="fa fa-bars"></i>
                                </button>
                            </div>
                        </div> 
                    </div>
                    <div class="no-relative col-lg-9 col-md-10">
                        <nav class="clearfix">
                            <button type="button" class="visible-xs-block btn-menu-close btn-link">
                                <i class="fa fa-times"></i>
                            </button>
                            <ul id="menu-main-menu" class="pull-left text-gotham">
                                <li class="menu-item">
                                    <a href="/brand">JUNGSAEMMOOL</a>
                                    <ul class="sub-menu" style="height: 200px;">
                                        <li class="menu-item"><a href="/brand">Brand JSM</a></li>
                                        <li class="menu-item"><a href="/artist">Artist JSM</a></li>
                                        <li class="menu-item"><a href="/artist/tip">ARTIST TIP</a></li>
                                        <li class="menu-item"><a href="/store">FINE STORE</a></li>


                                    </ul>
                                </li>

                                <li class="menu-item">
                                    <a href="/product">ALL PRODUCTS</a>
                                    <ul class="sub-menu" style="height: 200px;">
                                        <li class="menu-item"><a href="/product">BEST SELLERS</a></li>
                                        <li class="menu-item"><a href="/product">BASE</a></li>
                                        <li class="menu-item"><a href="/product">LIP</a></li>
                                        <li class="menu-item"><a href="/product">EYE</a></li>
                                        <li class="menu-item"><a href="/product">SKIPCARE</a></li>
                                        <li class="menu-item"><a href="/product">TOOL</a></li>



                                    </ul>
                                </li>
                                <li id="menu-item-1467" class="menu-item">
                                    <a href="">LOUNGE</a>
                                    <ul class="sub-menu" style="height: 200px;">
                                        <li class="menu-item"><a href="/event">EVENT STORE</a></li>
                                        <li class="menu-item"><a href="/linestory">LINE STORY</a></li>
                                        <!-- <li class="menu-item"><a href="">TEST</a></li> -->
                                        <!-- <li class="menu-item"><a href="">MEMBERSHIP</a></li> -->
                                    </ul>
                                </li>
                                <li class="menu-item">
                                    <a href="">Community</a>
                                    <ul class="sub-menu" style="height: 200px;">
                                        <li class="menu-item"><a href="/faq">FAQ</a></li>
                                        <li class="menu-item"><a href="/Notice">Notice</a></li>
                                        <li class="menu-item"><a href="/QA">Q&amp;A</a></li>
                                        <li class="menu-item"><a href="/review">REVIEW</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <button type="button" class="btn-header-search btn btn-default pull-right" data-toggle="modal" data-target="#juSearch">
                                <i class="fa fa-search"></i>
                            </button>
                            <div class="shadow" style="height: 185px;">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-2"></div>
                                    </div>
                                </div>
                            </div>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="juSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">TEST</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-search"></i> SEARCH</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="get" name="search">
                        <form role="search" method="get" class="woocommerce-product-search" >
                            <div class="input-group input-group-lg">
                                <input name="search" value="" class="MS_search_word form-control" placeholder="">
                                <span class="input-group-btn">
                                    <button onclick="" class="btn btn-primary" type="submit">SEARCH</button>
                                </span>
                            </div>
                            <!-- input-group -->
                            <input type="hidden" name="post_type" value="product">
                        </form>
                    </form>
                    <hr class="clear sm">
                </div>
            </div>
        </div>
    </div>
    <script>

    </script>
</header>