<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index()
    {
        return view('frontend.product');
    }
    public function bastseller()
    {
        return view('frontend.bestseller');
    }
    public function base()
    {
        return view('frontend.base');
    }
    public function lip()
    {
        return view('frontend.lip');
    }
    public function skincare()
    {
        return view('frontend.product');
    }
    public function tool()
    {
        return view('frontend.product');
    }
    public function show($id)
    {
        return view('frontend/Product.view1')->with(['ids' => $id]); 
    }
}
