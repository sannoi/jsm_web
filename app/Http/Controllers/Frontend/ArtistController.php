<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArtistController extends Controller
{
    public function index()
    {
        return view('frontend.artist');
    }

    public  function  artisttip ()
    {

        return view('frontend/ArtisTip.artisttip01');
    }

    public  function  artisttip02 ()
    {

        return view('frontend/ArtisTip.artisttip02');
    }

    public  function  detailartis ()
    {

        return view('frontend/ArtisTip.DetailArtistip');
    }
}
