<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    public function index()
    {
        return view('frontend\Lounge.Event');
    }
    public function Event2()
    {
        return view('frontend\Lounge.Event2');
    }
    public function Event2page2()
    {
        return view('frontend\Lounge.Event2page2');
    }
}
