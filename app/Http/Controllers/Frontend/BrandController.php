<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{
    public function index()
    {
        return view('frontend.brand');
    }

    public function magazines()
    {
        return view('frontend.magazines');
    }


}
