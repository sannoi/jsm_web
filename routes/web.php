<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Front//

Route::get('/brand', 'Frontend\BrandController@index');// หน้า Brand
Route::get('/brand/magazines/', 'Frontend\BrandController@magazines');

Route::get('/artist', 'Frontend\ArtistController@index');// หน้า artist
Route::get('/artist/tip', 'Frontend\ArtistController@artisttip');// หน้า artist tip
Route::get('/artist/tip2', 'Frontend\ArtistController@artisttip02');
Route::get('/Detail/Artis', 'Frontend\ArtistController@detailartis');// หน้า Detail Review
Route::get('/store', 'Frontend\StoreController@index');// หน้า store
Route::get('/Detail/Store', 'Frontend\StoreController@detailsore');// หน้า store


Route::get('/event', 'Frontend\EventController@index');// หน้า event
Route::get('/event2', 'Frontend\EventController@Event2');// หน้า event
Route::get('/event2page2', 'Frontend\EventController@Event2page2');// หน้า event
Route::get('/linestory', 'Frontend\LineStoryController@index');// หน้า Linestory
Route::get('/review', 'Frontend\ReviewController@index');// หน้า Review


/////////////////////////////////////////////////////////////////////////////////////////////////////////////

Route::get('/faq', 'Frontend\faqController@index');// หน้า faq
Route::get('/Notice', 'Frontend\NoticeController@index');// หน้า faq
Route::get('/QA', 'Frontend\QAController@index');// หน้า faq

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
Route::get('/product', 'Frontend\ProductController@index'); // หน้า all Product
Route::get('/product/detail/view/{id}', 'Frontend\ProductController@show'); // หน้า all Product
Route::get('/best', 'Frontend\ProductController@bastseller'); // หน้า all Product // bestseller
Route::get('/base', 'Frontend\ProductController@base'); // หน้า all Product // base
Route::get('/lip', 'Frontend\ProductController@lip'); // หน้า all Product // bestseller

